/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package emergencia;

import gestionAfiliado.RepositorioGestionAfiliados;
import gestionAsistenciaMedica.RepositorioDeAsistenciasMedicas;
import gestionEmpleados.RepositorioGestionEmpleados;
import gestionMoviles.RepositorioDeMoviles;
import gestionPagos.RepositorioGestionDePagos;


public class Empresa {
    
    private static Empresa empresa;
    private RepositorioGestionAfiliados gestionAfiliados;
    private RepositorioDeAsistenciasMedicas gestionAsistenciasMedicas;
    private RepositorioGestionEmpleados gestionEmpleados;
    private RepositorioDeMoviles gestionMoviles;
    private RepositorioGestionDePagos gestionPagos;
    
    
    
    public Empresa(){
        this.gestionAfiliados= new RepositorioGestionAfiliados();
        this.gestionAsistenciasMedicas=new RepositorioDeAsistenciasMedicas();
        this.gestionEmpleados = new RepositorioGestionEmpleados();
        this.gestionMoviles= new RepositorioDeMoviles();
        this.gestionPagos= new RepositorioGestionDePagos();
        
        
        
    }

    public static Empresa intancia(){
        
        if(empresa==null){
            empresa= new Empresa();
        }
        
        return empresa;
        
    }    
        
           
        



    public static Empresa getEmpresa() {
        return empresa;
    }

    public static void setEmpresa(Empresa empresa) {
        Empresa.empresa = empresa;
    }

    public RepositorioGestionAfiliados getGestionAfiliados() {
        return gestionAfiliados;
    }

    public void setGestionAfiliados(RepositorioGestionAfiliados gestionAfiliados) {
        this.gestionAfiliados = gestionAfiliados;
    }

    public RepositorioDeAsistenciasMedicas getGestionAsistenciasMedicas() {
        return gestionAsistenciasMedicas;
    }

    public void setGestionAsistenciasMedicas(RepositorioDeAsistenciasMedicas gestionAsistenciasMedicas) {
        this.gestionAsistenciasMedicas = gestionAsistenciasMedicas;
    }

    public RepositorioGestionEmpleados getGestionEmpleados() {
        return gestionEmpleados;
    }

    public void setGestionEmpleados(RepositorioGestionEmpleados gestionEmpleados) {
        this.gestionEmpleados = gestionEmpleados;
    }

    public RepositorioDeMoviles getGestionMoviles() {
        return gestionMoviles;
    }

    public void setGestionMoviles(RepositorioDeMoviles gestionMoviles) {
        this.gestionMoviles = gestionMoviles;
    }

    public RepositorioGestionDePagos getGestionPagos() {
        return gestionPagos;
    }

    public void setGestionPagos(RepositorioGestionDePagos gestionPagos) {
        this.gestionPagos = gestionPagos;
    }


    
    
}
