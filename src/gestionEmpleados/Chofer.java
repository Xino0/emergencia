/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionEmpleados;


import gestionAfiliado.Genero;
import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author david
 */
public class Chofer extends Empleado{
    private Integer licenciadeConducir;
    private String tipoDeLicencia;

    public Chofer(Integer licenciadeConducir, String tipoDeLicencia, Integer numeroAgente, Boolean disponibilidad, Turno guardia, String nombre, LocalDate fechaDeNacimiento, String dni, Genero sexo, String telefono, String email, String direccion) {
        super(numeroAgente, disponibilidad, guardia, nombre, fechaDeNacimiento, dni, sexo, telefono, email, direccion);
        this.licenciadeConducir = licenciadeConducir;
        this.tipoDeLicencia = tipoDeLicencia;
    }

    
    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Chofer other = (Chofer) obj;
        if (!Objects.equals(this.tipoDeLicencia, other.tipoDeLicencia)) {
            return false;
        }
        return Objects.equals(this.licenciadeConducir, other.licenciadeConducir);
    }
    
    
   

    

    public Integer getLicenciadeConducir() {
        return licenciadeConducir;
    }

    public String getTipoDeLicencia() {
        return tipoDeLicencia;
    }

    @Override
    public String toString() {
            
        return super.toString()+" licencia de Conducir: " + licenciadeConducir + " Tipo De Licencia: " + tipoDeLicencia ;
    }
    
    
    
}
