/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionEmpleados;


import gestionAfiliado.Persona;
import gestionAfiliado.Genero;
import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author david
 */
public class Empleado extends Persona{
    private Integer numeroAgente;
    private Boolean disponibilidad;
    private Turno guardia;

    public Empleado(Integer numeroAgente, Boolean disponibilidad, Turno guardia, String nombre, LocalDate fechaDeNacimiento, String dni, Genero sexo, String telefono, String email, String direccion) {
        super(nombre, fechaDeNacimiento, dni, sexo, telefono, email, direccion);
        this.numeroAgente = numeroAgente;
        this.disponibilidad = disponibilidad;
        this.guardia = guardia;
    }

   

    public Integer getNumeroAgente() {
        return numeroAgente;
    }

    public void setNumeroAgente(Integer numeroAgente) {
        this.numeroAgente = numeroAgente;
    }

    public Boolean getDisponibilidad() {
        return disponibilidad;
    }

    public void setDisponibilidad(Boolean disponibilidad) {
        this.disponibilidad = disponibilidad;
    }
    
    public void changeDisponibilidad(){
        if(disponibilidad == true){
            disponibilidad = false;
        }else{
            disponibilidad = true;
        }
    }
    
    
    
    public Turno getGuardia() {
        return guardia;
    }

    public void setGuardia(Turno guardia) {
        this.guardia = guardia;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.numeroAgente);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Empleado other = (Empleado) obj;
        return Objects.equals(this.numeroAgente, other.numeroAgente);
    }

    
  

   
    @Override
    public String toString() {
        return super.toString()+" N°Agente:" + numeroAgente  ;
    }
    
    
    
    
    
}
