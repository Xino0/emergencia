/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionEmpleados;


import gestionAfiliado.Genero;
import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author david
 */
public class Enfermero extends Empleado {
    private String especialidad;
    private Integer nroMatricula;

    public Enfermero(String especialidad, Integer nroMatricula, Integer numeroAgente, Boolean disponibilidad, Turno guardia, String nombre, LocalDate fechaDeNacimiento, String dni, Genero sexo, String telefono, String email, String direccion) {
        super(numeroAgente, disponibilidad, guardia, nombre, fechaDeNacimiento, dni, sexo, telefono, email, direccion);
        this.especialidad = especialidad;
        this.nroMatricula = nroMatricula;
    }

    

    

   

    public String getEspecialidad() {
        return especialidad;
    }

    public Integer getNroMatricula() {
        return nroMatricula;
    }

    @Override
    public String toString() {
        return super.toString()+ " N° Matricula: " + nroMatricula + " Especialidad: " + especialidad;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + Objects.hashCode(this.especialidad);
        hash = 41 * hash + Objects.hashCode(this.nroMatricula);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Enfermero other = (Enfermero) obj;
        if (!Objects.equals(this.especialidad, other.especialidad)) {
            return false;
        }
        return Objects.equals(this.nroMatricula, other.nroMatricula);
    }

   
    
    
}
