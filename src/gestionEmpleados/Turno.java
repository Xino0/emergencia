/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionEmpleados;
import java.time.LocalDate;

import java.util.Objects;
import java.util.Random;
/**
 *
 * @author david
 */
public class Turno {
    private LocalDate fecha;
    private int horaLlegada;
    private int horaSalida;

    public Turno() {
        Random rand = new Random();
        this.fecha = LocalDate.now().plusDays(rand.nextInt(30)); // fecha aleatoria dentro de los próximos 30 días
        this.horaLlegada = rand.nextInt(17); // hora aleatoria del día entre 0 y 16
        this.horaSalida = this.horaLlegada + 8; // turno de 8 horas
    }

    
    
    public boolean estaDisponible(LocalDate fechaTrabajo, int hora) {
        // Comprueba si la fecha coincide
        if (!fecha.isEqual(fechaTrabajo)) {
            return false;
        }
        
        // Comprueba si la hora está dentro del rango de llegada y salida
        else if (hora >= horaLlegada && hora <= horaSalida) {
            return true;
        }

        return false;
    }
    
    
    
    
    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }
    
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Turno other = (Turno) obj;
        if (this.horaLlegada != other.horaLlegada) {
            return false;
        }
        if (this.horaSalida != other.horaSalida) {
            return false;
        }
        return Objects.equals(this.fecha, other.fecha);
    }

    @Override
    public String toString() {
        return "Turno{" + "fecha=" + fecha + ", horaLlegada=" + horaLlegada + ", horaSalida=" + horaSalida + '}';
    }
    
    
    
}
