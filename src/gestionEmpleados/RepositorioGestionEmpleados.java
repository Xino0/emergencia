/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionEmpleados;

import Excepciones.AltaException;
import Excepciones.InexistenteException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author david
 */
public class RepositorioGestionEmpleados {
    private List<Empleado> empleados;

    public RepositorioGestionEmpleados() {
        this.empleados = new ArrayList<>();
    }

    public List<Empleado> getEmpleados() {
        return empleados;
    }
    
    
    
    
    public void agregarEmpleado(Empleado agregar) throws AltaException, InexistenteException{
      
        Empleado busqueda=this.buscar(agregar);
        
        if(busqueda==null){
            
            empleados.add(agregar);
            
        }
        else if(busqueda.equals(agregar)){
         
            throw new AltaException("El empleado ya se encuentra registrado en el sistema");
        }
       
        else  empleados.add(agregar);
      
        

         
        
        

        
    }
    
    
    public void eliminarEmpleado(Empleado eliminar)throws InexistenteException{
        Empleado buscar=this.buscar(eliminar);
        if(buscar==null){
            throw new InexistenteException("El Empleado a eliminar no existe.");
        }
        else{
            System.out.println("Se elimino el Empleado: "+eliminar.getNombre());
            empleados.remove(eliminar);
        }
            
    }
    
   
    
    public Empleado buscar(Empleado buscar) throws InexistenteException{
        
        if(buscar!=null){
            
            for(Empleado aux: empleados){ 
                 
                 if(buscar.equals(aux)){
                    
                    return buscar;
                 
                 }
            }
    
        }
        else if(buscar==null){
             throw new InexistenteException("Empleado Inexistente"); 
        }
            
         return null;            
        
    }
    public Empleado buscar(Integer buscar) throws InexistenteException,AltaException{
        
        if(buscar!=null){
            
            
            for(Empleado aux: empleados){ 
                
                 if(buscar.equals(aux.getNumeroAgente()) && (aux.getDisponibilidad()==true)){
                    
                    return aux;
                 
                }
                 else if(buscar.equals(aux.getNumeroAgente()) && (aux.getDisponibilidad()==false)){
                      throw new AltaException("El "+aux.getClass().getSimpleName()+" No esta disponible.");

                 }
                
            }
    
        }
        else if(buscar==null){
             throw new InexistenteException("Empleado Inexistente"); 
        }
            
         return null;            
        
    }
    
    public void modificarEmpleado(Empleado Original, Empleado Modificar) throws InexistenteException {
        
        Empleado buscar = this.buscar(Original);
        if(buscar == null){
            throw new InexistenteException("El Empleado a modificar no existe.");
        }
        else{
            int index = empleados.indexOf(buscar);
            empleados.set(index, Modificar);
            System.out.println("Se modificó el empleado: " + Modificar.getNombre());
        }
        
    }
    
    
    
    
}
