/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package GUI.Empleado;

import Excepciones.AltaException;
import Excepciones.InexistenteException;
import emergencia.Empresa;
import gestionAfiliado.Genero;
import gestionEmpleados.Doctor;
import gestionEmpleados.Empleado;
import gestionEmpleados.Turno;
import java.awt.Color;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

/**
 *
 * @author david
 */
public class BajaEmpleado extends javax.swing.JDialog {
    private final Empresa gestionEmpresa;

  
    public BajaEmpleado(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        gestionEmpresa=Empresa.intancia();
        
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        numeroAgenteTxt = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        nombreLabel = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(204, 204, 204));

        jLabel2.setFont(new java.awt.Font("Swis721 Ex BT", 1, 24)); // NOI18N
        jLabel2.setText("Baja de Empleado");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(187, Short.MAX_VALUE)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 430, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(73, 73, 73))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 18, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 20, 690, 80));

        jPanel4.setBackground(new java.awt.Color(0, 204, 204));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/icono versiofinal.png"))); // NOI18N

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(jLabel1)
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1)
                .addContainerGap(460, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(-10, 0, 220, 630));

        jPanel5.setBackground(new java.awt.Color(0, 204, 204));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 160, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 630, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(880, 0, 160, 630));

        numeroAgenteTxt.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        numeroAgenteTxt.setForeground(new java.awt.Color(204, 204, 204));
        numeroAgenteTxt.setText("Ingrese un  numero de agente");
        numeroAgenteTxt.setBorder(null);
        numeroAgenteTxt.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                numeroAgenteTxtMousePressed(evt);
            }
        });
        numeroAgenteTxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                numeroAgenteTxtActionPerformed(evt);
            }
        });
        numeroAgenteTxt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                numeroAgenteTxtKeyTyped(evt);
            }
        });
        jPanel1.add(numeroAgenteTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 210, 410, 30));

        jSeparator1.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 240, 410, 20));

        nombreLabel.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        nombreLabel.setText("N° AGENTE");
        jPanel1.add(nombreLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 220, -1, -1));

        jButton1.setBackground(new java.awt.Color(0, 204, 204));
        jButton1.setText("Eliminar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 290, 172, 41));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 1034, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 631, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        
        try {
            Empleado buscar = gestionEmpresa.getGestionEmpleados().buscar(Integer.parseInt(numeroAgenteTxt.getText()));
            gestionEmpresa.getGestionEmpleados().eliminarEmpleado(buscar);
            this.dispose();
        } catch (InexistenteException ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage());
        } catch (AltaException ex) {
            Logger.getLogger(BajaEmpleado.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_jButton1ActionPerformed

    private void numeroAgenteTxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_numeroAgenteTxtActionPerformed

    }//GEN-LAST:event_numeroAgenteTxtActionPerformed

    private void numeroAgenteTxtMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_numeroAgenteTxtMousePressed
        if (numeroAgenteTxt.getText().equals("Ingrese un  numero de agente")) {
            numeroAgenteTxt.setText("");
            numeroAgenteTxt.setForeground(Color.black);
        }
      
        

    }//GEN-LAST:event_numeroAgenteTxtMousePressed

    private void numeroAgenteTxtKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_numeroAgenteTxtKeyTyped
        char c = evt.getKeyChar();
        if (c<'0' || c>'9') evt.consume();
    }//GEN-LAST:event_numeroAgenteTxtKeyTyped
    
    
    

    private int GeneradorNumeroAgente() {
        int  limiteInferior = 1000;
        int  limiteSuperior = 9999;
        Random rand = new Random();
        return rand.nextInt(limiteSuperior - limiteInferior + 1) + limiteInferior;
    }



    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel nombreLabel;
    private javax.swing.JTextField numeroAgenteTxt;
    // End of variables declaration//GEN-END:variables
}
