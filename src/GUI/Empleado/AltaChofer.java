/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package GUI.Empleado;

import Excepciones.AltaException;
import Excepciones.InexistenteException;
import emergencia.Empresa;
import gestionAfiliado.Genero;
import gestionEmpleados.Chofer;

import gestionEmpleados.Turno;
import java.awt.Color;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Random;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

/**
 *
 * @author david
 */
public class AltaChofer extends javax.swing.JDialog {
    private final Empresa gestionEmpresa;

  
    public AltaChofer(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        gestionEmpresa=Empresa.intancia();
        cargarComboBox();
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        tipoDeLicenciaLabel = new javax.swing.JLabel();
        nombreTxt = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        dniTxt = new javax.swing.JTextField();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        tipoDeLicenciaTxt = new javax.swing.JTextField();
        nombreLabel = new javax.swing.JLabel();
        dniLabel = new javax.swing.JLabel();
        fechaNacimientoLabel = new javax.swing.JLabel();
        sexoLabel = new javax.swing.JLabel();
        numeroDeLicenciaLabel = new javax.swing.JLabel();
        numeroDeLicenciaTxt = new javax.swing.JTextField();
        jSeparator6 = new javax.swing.JSeparator();
        jSeparator7 = new javax.swing.JSeparator();
        emailTxt = new javax.swing.JTextField();
        numeroTelefonoTxt = new javax.swing.JTextField();
        jSeparator8 = new javax.swing.JSeparator();
        direccionLabel = new javax.swing.JLabel();
        numeroTelefonoLabel = new javax.swing.JLabel();
        fechaNacimientoTxt = new javax.swing.JFormattedTextField();
        jComboSexo = new javax.swing.JComboBox<>();
        direccionLabel1 = new javax.swing.JLabel();
        direccionTxt = new javax.swing.JTextField();
        jSeparator9 = new javax.swing.JSeparator();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(204, 204, 204));

        jLabel2.setFont(new java.awt.Font("Swis721 Ex BT", 1, 24)); // NOI18N
        jLabel2.setText("Registro Chofer");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(226, 226, 226)
                .addComponent(jLabel2)
                .addContainerGap(249, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 18, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 20, 700, 80));

        jPanel4.setBackground(new java.awt.Color(0, 204, 204));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/icono versiofinal.png"))); // NOI18N

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(jLabel1)
                .addContainerGap(51, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jLabel1)
                .addContainerGap(459, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 240, 630));

        jPanel5.setBackground(new java.awt.Color(0, 204, 204));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 240, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 630, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(920, 0, -1, 630));

        tipoDeLicenciaLabel.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        tipoDeLicenciaLabel.setText("TIPO DE LICENCIA");
        jPanel1.add(tipoDeLicenciaLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 320, -1, -1));

        nombreTxt.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        nombreTxt.setForeground(new java.awt.Color(204, 204, 204));
        nombreTxt.setText("Ingrese su nombre");
        nombreTxt.setBorder(null);
        nombreTxt.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                nombreTxtMousePressed(evt);
            }
        });
        nombreTxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nombreTxtActionPerformed(evt);
            }
        });
        jPanel1.add(nombreTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 110, 410, 30));

        jSeparator1.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 140, 410, 20));

        jSeparator2.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 190, 410, 20));

        dniTxt.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        dniTxt.setForeground(new java.awt.Color(204, 204, 204));
        dniTxt.setText("Ingrese su Dni");
        dniTxt.setBorder(null);
        dniTxt.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                dniTxtMousePressed(evt);
            }
        });
        dniTxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dniTxtActionPerformed(evt);
            }
        });
        dniTxt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                dniTxtKeyTyped(evt);
            }
        });
        jPanel1.add(dniTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 160, 410, 30));

        jSeparator3.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 240, 410, 20));

        jSeparator4.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 290, 410, 20));

        jSeparator5.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator5, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 390, 410, 20));

        tipoDeLicenciaTxt.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        tipoDeLicenciaTxt.setForeground(new java.awt.Color(204, 204, 204));
        tipoDeLicenciaTxt.setText("Ingrese el tipo de licencia");
        tipoDeLicenciaTxt.setBorder(null);
        tipoDeLicenciaTxt.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tipoDeLicenciaTxtMousePressed(evt);
            }
        });
        tipoDeLicenciaTxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tipoDeLicenciaTxtActionPerformed(evt);
            }
        });
        jPanel1.add(tipoDeLicenciaTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 310, 410, 30));

        nombreLabel.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        nombreLabel.setText("NOMBRE");
        jPanel1.add(nombreLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 120, -1, -1));

        dniLabel.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        dniLabel.setText("DNI");
        jPanel1.add(dniLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 170, -1, -1));

        fechaNacimientoLabel.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        fechaNacimientoLabel.setText("FECHA/NACIMIENTO");
        jPanel1.add(fechaNacimientoLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 220, -1, -1));

        sexoLabel.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        sexoLabel.setText("SEXO");
        jPanel1.add(sexoLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 270, -1, -1));

        numeroDeLicenciaLabel.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        numeroDeLicenciaLabel.setText("NRO DE LICENCIA");
        jPanel1.add(numeroDeLicenciaLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 370, -1, -1));

        numeroDeLicenciaTxt.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        numeroDeLicenciaTxt.setForeground(new java.awt.Color(204, 204, 204));
        numeroDeLicenciaTxt.setText("Ingrese su numero de licencia");
        numeroDeLicenciaTxt.setBorder(null);
        numeroDeLicenciaTxt.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                numeroDeLicenciaTxtMousePressed(evt);
            }
        });
        numeroDeLicenciaTxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                numeroDeLicenciaTxtActionPerformed(evt);
            }
        });
        numeroDeLicenciaTxt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                numeroDeLicenciaTxtKeyTyped(evt);
            }
        });
        jPanel1.add(numeroDeLicenciaTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 360, 410, 30));

        jSeparator6.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator6, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 340, 410, 20));

        jSeparator7.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator7, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 490, 410, 20));

        emailTxt.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        emailTxt.setForeground(new java.awt.Color(204, 204, 204));
        emailTxt.setText("Ingrese su email");
        emailTxt.setBorder(null);
        emailTxt.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                emailTxtMousePressed(evt);
            }
        });
        emailTxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                emailTxtActionPerformed(evt);
            }
        });
        jPanel1.add(emailTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 460, 410, 30));

        numeroTelefonoTxt.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        numeroTelefonoTxt.setForeground(new java.awt.Color(204, 204, 204));
        numeroTelefonoTxt.setText("Ingrese su telefono");
        numeroTelefonoTxt.setBorder(null);
        numeroTelefonoTxt.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                numeroTelefonoTxtMousePressed(evt);
            }
        });
        numeroTelefonoTxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                numeroTelefonoTxtActionPerformed(evt);
            }
        });
        numeroTelefonoTxt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                numeroTelefonoTxtKeyTyped(evt);
            }
        });
        jPanel1.add(numeroTelefonoTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 410, 410, 30));

        jSeparator8.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator8, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 440, 410, 20));

        direccionLabel.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        direccionLabel.setText("EMAIL");
        jPanel1.add(direccionLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 470, -1, -1));

        numeroTelefonoLabel.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        numeroTelefonoLabel.setText("NRO. TELEFONO");
        jPanel1.add(numeroTelefonoLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 420, -1, -1));

        fechaNacimientoTxt.setBorder(null);
        fechaNacimientoTxt.setForeground(new java.awt.Color(204, 204, 204));
        fechaNacimientoTxt.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter(java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT))));
        fechaNacimientoTxt.setText("d/M/yy");
        fechaNacimientoTxt.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                fechaNacimientoTxtMousePressed(evt);
            }
        });
        fechaNacimientoTxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fechaNacimientoTxtActionPerformed(evt);
            }
        });
        jPanel1.add(fechaNacimientoTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 210, 420, 30));

        jComboSexo.setForeground(new java.awt.Color(204, 204, 204));
        jComboSexo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jComboSexo.setAutoscrolls(true);
        jComboSexo.setBorder(null);
        jComboSexo.setDebugGraphicsOptions(javax.swing.DebugGraphics.NONE_OPTION);
        jPanel1.add(jComboSexo, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 260, 410, 30));

        direccionLabel1.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        direccionLabel1.setText("DIRECCION");
        jPanel1.add(direccionLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 510, -1, -1));

        direccionTxt.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        direccionTxt.setForeground(new java.awt.Color(204, 204, 204));
        direccionTxt.setText("Ingrese su direccion");
        direccionTxt.setBorder(null);
        direccionTxt.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                direccionTxtMousePressed(evt);
            }
        });
        direccionTxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                direccionTxtActionPerformed(evt);
            }
        });
        jPanel1.add(direccionTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 500, 410, 30));

        jSeparator9.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator9, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 530, 410, 20));

        jButton1.setBackground(new java.awt.Color(0, 204, 204));
        jButton1.setText("Agregar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 560, 171, 41));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1145, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 631, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void nombreTxtMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_nombreTxtMousePressed
        if (nombreTxt.getText().equals("Ingrese su nombre")) {
            nombreTxt.setText("");
            nombreTxt.setForeground(Color.black);
        } 
        if (dniTxt.getText().isEmpty()) {
            dniTxt.setText("Ingrese su Dni");
            dniTxt.setForeground(Color.gray);
        }
        if (fechaNacimientoTxt.getText().isEmpty()) {
            fechaNacimientoTxt.setText("d/M/yy");
            fechaNacimientoTxt.setForeground(Color.gray);
        }
        if (tipoDeLicenciaTxt.getText().isEmpty()) {
            tipoDeLicenciaTxt.setText("Ingrese el tipo de licencia");
            tipoDeLicenciaTxt.setForeground(Color.gray);
        }
        if (numeroDeLicenciaTxt.getText().isEmpty()) {
            numeroDeLicenciaTxt.setText("Ingrese su numero de licencia");
            numeroDeLicenciaTxt.setForeground(Color.gray);
        }
        if (numeroTelefonoTxt.getText().isEmpty()) {
            numeroTelefonoTxt.setText("Ingrese su telefono");
            numeroTelefonoTxt.setForeground(Color.gray);
        }
        if (emailTxt.getText().isEmpty()) {
            emailTxt.setText("Ingrese su email");
            emailTxt.setForeground(Color.gray);
        }
        if (direccionTxt.getText().isEmpty()) {
            direccionTxt.setText("Ingrese su direccion");
            direccionTxt.setForeground(Color.gray);
        }
        
        
        
    }//GEN-LAST:event_nombreTxtMousePressed

    private void dniTxtMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dniTxtMousePressed
        if (dniTxt.getText().equals("Ingrese su Dni")) {
            dniTxt.setText("");
            dniTxt.setForeground(Color.black);
        }
        if (nombreTxt.getText().isEmpty()) {
            nombreTxt.setText("Ingrese su nombre");
            nombreTxt.setForeground(Color.gray);
        }   
        if (fechaNacimientoTxt.getText().isEmpty()) {
            fechaNacimientoTxt.setText("d/M/yy");
            fechaNacimientoTxt.setForeground(Color.gray);
        }
        if (tipoDeLicenciaTxt.getText().isEmpty()) {
            tipoDeLicenciaTxt.setText("Ingrese el tipo de licencia");
            tipoDeLicenciaTxt.setForeground(Color.gray);
        }
        if (numeroDeLicenciaTxt.getText().isEmpty()) {
            numeroDeLicenciaTxt.setText("Ingrese su numero de licencia");
            numeroDeLicenciaTxt.setForeground(Color.gray);
        }
        if (numeroTelefonoTxt.getText().isEmpty()) {
            numeroTelefonoTxt.setText("Ingrese su telefono");
            numeroTelefonoTxt.setForeground(Color.gray);
        }
        if (emailTxt.getText().isEmpty()) {
            emailTxt.setText("Ingrese su email");
            emailTxt.setForeground(Color.gray);
        }
        if (direccionTxt.getText().isEmpty()) {
            direccionTxt.setText("Ingrese su direccion");
            direccionTxt.setForeground(Color.gray);
        }
        
        
        
    }//GEN-LAST:event_dniTxtMousePressed

    private void tipoDeLicenciaTxtMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tipoDeLicenciaTxtMousePressed
        if (tipoDeLicenciaTxt.getText().equals("Ingrese el tipo de licencia")) {
            tipoDeLicenciaTxt.setText("");
            tipoDeLicenciaTxt.setForeground(Color.black);
        }
        if (nombreTxt.getText().isEmpty()) {
            nombreTxt.setText("Ingrese su nombre");
            nombreTxt.setForeground(Color.gray);
        }
        if (dniTxt.getText().isEmpty()) {
            dniTxt.setText("Ingrese su Dni");
            dniTxt.setForeground(Color.gray);
        }
        if (fechaNacimientoTxt.getText().isEmpty()) {
            fechaNacimientoTxt.setText("d/M/yy");
            fechaNacimientoTxt.setForeground(Color.gray);
        }     
        if (numeroDeLicenciaTxt.getText().isEmpty()) {
            numeroDeLicenciaTxt.setText("Ingrese su numero de licencia");
            numeroDeLicenciaTxt.setForeground(Color.gray);
        }
        if (numeroTelefonoTxt.getText().isEmpty()) {
            numeroTelefonoTxt.setText("Ingrese su telefono");
            numeroTelefonoTxt.setForeground(Color.gray);
        }
        if (emailTxt.getText().isEmpty()) {
            emailTxt.setText("Ingrese su email");
            emailTxt.setForeground(Color.gray);
        }
        if (direccionTxt.getText().isEmpty()) {
            direccionTxt.setText("Ingrese su direccion");
            direccionTxt.setForeground(Color.gray);
        }
        
        
    }//GEN-LAST:event_tipoDeLicenciaTxtMousePressed

    private void nombreTxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nombreTxtActionPerformed

    }//GEN-LAST:event_nombreTxtActionPerformed

    private void numeroDeLicenciaTxtMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_numeroDeLicenciaTxtMousePressed
        if (numeroDeLicenciaTxt.getText().equals("Ingrese su numero de licencia")) {
            numeroDeLicenciaTxt.setText("");
            numeroDeLicenciaTxt.setForeground(Color.black);
        }
        if (nombreTxt.getText().isEmpty()) {
            nombreTxt.setText("Ingrese su nombre");
            nombreTxt.setForeground(Color.gray);
        }
        if (dniTxt.getText().isEmpty()) {
            dniTxt.setText("Ingrese su Dni");
            dniTxt.setForeground(Color.gray);
        }
        if (fechaNacimientoTxt.getText().isEmpty()) {
            fechaNacimientoTxt.setText("d/M/yy");
            fechaNacimientoTxt.setForeground(Color.gray);
        }
        if (tipoDeLicenciaTxt.getText().isEmpty()) {
            tipoDeLicenciaTxt.setText("Ingrese el tipo de licencia");
            tipoDeLicenciaTxt.setForeground(Color.gray);
        }    
        if (numeroTelefonoTxt.getText().isEmpty()) {
            numeroTelefonoTxt.setText("Ingrese su telefono");
            numeroTelefonoTxt.setForeground(Color.gray);
        }
        if (emailTxt.getText().isEmpty()) {
            emailTxt.setText("Ingrese su email");
            emailTxt.setForeground(Color.gray);
        }
        if (direccionTxt.getText().isEmpty()) {
            direccionTxt.setText("Ingrese su direccion");
            direccionTxt.setForeground(Color.gray);
        }
    }//GEN-LAST:event_numeroDeLicenciaTxtMousePressed

    private void numeroDeLicenciaTxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_numeroDeLicenciaTxtActionPerformed
   
    }//GEN-LAST:event_numeroDeLicenciaTxtActionPerformed

    private void emailTxtMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_emailTxtMousePressed
        if (emailTxt.getText().equals("Ingrese su email")) {
            emailTxt.setText("");
            emailTxt.setForeground(Color.black);
        }
        if (nombreTxt.getText().isEmpty()) {
            nombreTxt.setText("Ingrese su nombre");
            nombreTxt.setForeground(Color.gray);
        }
        if (dniTxt.getText().isEmpty()) {
            dniTxt.setText("Ingrese su Dni");
            dniTxt.setForeground(Color.gray);
        }
        if (fechaNacimientoTxt.getText().isEmpty()) {
            fechaNacimientoTxt.setText("d/M/yy");
            fechaNacimientoTxt.setForeground(Color.gray);
        }
        if (tipoDeLicenciaTxt.getText().isEmpty()) {
            tipoDeLicenciaTxt.setText("Ingrese el tipo de licencia");
            tipoDeLicenciaTxt.setForeground(Color.gray);
        }
        if (numeroDeLicenciaTxt.getText().isEmpty()) {
            numeroDeLicenciaTxt.setText("Ingrese su numero de licencia");
            numeroDeLicenciaTxt.setForeground(Color.gray);
        }
        if (numeroTelefonoTxt.getText().isEmpty()) {
            numeroTelefonoTxt.setText("Ingrese su telefono");
            numeroTelefonoTxt.setForeground(Color.gray);
        }
        if (direccionTxt.getText().isEmpty()) {
            direccionTxt.setText("Ingrese su direccion");
            direccionTxt.setForeground(Color.gray);
        }
        
        
        
        
    }//GEN-LAST:event_emailTxtMousePressed

    private void emailTxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_emailTxtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_emailTxtActionPerformed

    private void numeroTelefonoTxtMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_numeroTelefonoTxtMousePressed
        if (numeroTelefonoTxt.getText().equals("Ingrese su telefono")) {
            numeroTelefonoTxt.setText("");
            numeroTelefonoTxt.setForeground(Color.black);
        }
        if (nombreTxt.getText().isEmpty()) {
            nombreTxt.setText("Ingrese su nombre");
            nombreTxt.setForeground(Color.gray);
        }
        if (dniTxt.getText().isEmpty()) {
            dniTxt.setText("Ingrese su Dni");
            dniTxt.setForeground(Color.gray);
        }  
        if (fechaNacimientoTxt.getText().isEmpty()) {
            fechaNacimientoTxt.setText("d/M/yy");
            fechaNacimientoTxt.setForeground(Color.gray);
        }
        if (tipoDeLicenciaTxt.getText().isEmpty()) {
            tipoDeLicenciaTxt.setText("Ingrese el tipo de licencia");
            tipoDeLicenciaTxt.setForeground(Color.gray);
        }
        if (numeroDeLicenciaTxt.getText().isEmpty()) {
            numeroDeLicenciaTxt.setText("Ingrese su numero de licencia");
            numeroDeLicenciaTxt.setForeground(Color.gray);
        }     
        if (emailTxt.getText().isEmpty()) {
            emailTxt.setText("Ingrese su email");
            emailTxt.setForeground(Color.gray);
        }
        if (direccionTxt.getText().isEmpty()) {
            direccionTxt.setText("Ingrese su direccion");
            direccionTxt.setForeground(Color.gray);
        }
        
    }//GEN-LAST:event_numeroTelefonoTxtMousePressed

    private void dniTxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dniTxtActionPerformed
     
    }//GEN-LAST:event_dniTxtActionPerformed

    private void fechaNacimientoTxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fechaNacimientoTxtActionPerformed
     
    }//GEN-LAST:event_fechaNacimientoTxtActionPerformed

    private void tipoDeLicenciaTxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tipoDeLicenciaTxtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tipoDeLicenciaTxtActionPerformed

    private void numeroTelefonoTxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_numeroTelefonoTxtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_numeroTelefonoTxtActionPerformed

    private void fechaNacimientoTxtMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_fechaNacimientoTxtMousePressed
        if (fechaNacimientoTxt.getText().equals("d/M/yy")) {
            fechaNacimientoTxt.setText("");
           fechaNacimientoTxt.setForeground(Color.black);
        }
        if (nombreTxt.getText().isEmpty()) {
            nombreTxt.setText("Ingrese su nombre");
            nombreTxt.setForeground(Color.gray);
        }
        if (dniTxt.getText().isEmpty()) {
            dniTxt.setText("Ingrese su Dni");
            dniTxt.setForeground(Color.gray);
        }       
        if (tipoDeLicenciaTxt.getText().isEmpty()) {
            tipoDeLicenciaTxt.setText("Ingrese el tipo de licencia");
            tipoDeLicenciaTxt.setForeground(Color.gray);
        }
        if (numeroDeLicenciaTxt.getText().isEmpty()) {
            numeroDeLicenciaTxt.setText("Ingrese su numero de matricula");
            numeroDeLicenciaTxt.setForeground(Color.gray);
        }
        if (numeroTelefonoTxt.getText().isEmpty()) {
            numeroTelefonoTxt.setText("Ingrese su telefono");
            numeroTelefonoTxt.setForeground(Color.gray);
        }
        if (emailTxt.getText().isEmpty()) {
            emailTxt.setText("Ingrese su email");
            emailTxt.setForeground(Color.gray);
        }
        if (direccionTxt.getText().isEmpty()) {
            direccionTxt.setText("Ingrese su direccion");
            direccionTxt.setForeground(Color.gray);
        }
    }//GEN-LAST:event_fechaNacimientoTxtMousePressed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        Integer numeroAgente= GeneradorNumeroAgente();
        
       
        Turno nuevoTurno= new Turno();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/M/yy");
        LocalDate fechaNacimiento = LocalDate.parse(fechaNacimientoTxt.getText(), formatter);
        Chofer nuevo= new Chofer(Integer.parseInt(numeroDeLicenciaTxt.getText()),tipoDeLicenciaTxt.getText(),numeroAgente,true,nuevoTurno,nombreTxt.getText(),fechaNacimiento,dniTxt.getText(), (Genero) jComboSexo.getSelectedItem(),numeroTelefonoTxt.getText(),emailTxt.getText(),direccionTxt.getText());

        try {
            gestionEmpresa.getGestionEmpleados().agregarEmpleado(nuevo);
            this.dispose();
        } catch (AltaException | InexistenteException ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage());
        }
        
        
        
        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void direccionTxtMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_direccionTxtMousePressed
        if (direccionTxt.getText().equals("Ingrese su direccion")) {
            direccionTxt.setText("");
            direccionTxt.setForeground(Color.black);
        }
        if (nombreTxt.getText().isEmpty()) {
            nombreTxt.setText("Ingrese su nombre");
            nombreTxt.setForeground(Color.gray);
        }
        if (dniTxt.getText().isEmpty()) {
            dniTxt.setText("Ingrese su Dni");
            dniTxt.setForeground(Color.gray);
        }
        if (fechaNacimientoTxt.getText().isEmpty()) {
            fechaNacimientoTxt.setText("d/M/yy");
            fechaNacimientoTxt.setForeground(Color.gray);
        }
        if (tipoDeLicenciaTxt.getText().isEmpty()) {
            tipoDeLicenciaTxt.setText("Ingrese el tipo de licencia");
            tipoDeLicenciaTxt.setForeground(Color.gray);
        }
        if (numeroDeLicenciaTxt.getText().isEmpty()) {
            numeroDeLicenciaTxt.setText("Ingrese su numero de matricula");
            numeroDeLicenciaTxt.setForeground(Color.gray);
        }
        if (numeroTelefonoTxt.getText().isEmpty()) {
            numeroTelefonoTxt.setText("Ingrese su telefono");
            numeroTelefonoTxt.setForeground(Color.gray);
        }
        if (emailTxt.getText().isEmpty())  {
            emailTxt.setText("Ingrese su email");
            emailTxt.setForeground(Color.gray);
        }
    }//GEN-LAST:event_direccionTxtMousePressed

    private void direccionTxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_direccionTxtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_direccionTxtActionPerformed

    private void dniTxtKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dniTxtKeyTyped
        char c = evt.getKeyChar();
        if (c<'0' || c>'9') evt.consume();
    }//GEN-LAST:event_dniTxtKeyTyped

    private void numeroDeLicenciaTxtKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_numeroDeLicenciaTxtKeyTyped
        char c = evt.getKeyChar();
        if (c<'0' || c>'9') evt.consume();
    }//GEN-LAST:event_numeroDeLicenciaTxtKeyTyped

    private void numeroTelefonoTxtKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_numeroTelefonoTxtKeyTyped
        char c = evt.getKeyChar();
        if (c<'0' || c>'9') evt.consume();
    }//GEN-LAST:event_numeroTelefonoTxtKeyTyped
    
    private void cargarComboBox(){
        
        JComboBox<Genero> miComboBox = new JComboBox<>();
        jComboSexo.setModel(new DefaultComboBoxModel(Genero.values()));
    }
    

    private Integer GeneradorNumeroAgente() {
        Integer  limiteInferior = 1000;
        Integer  limiteSuperior = 9999;
        Random rand = new Random();
        return rand.nextInt(limiteSuperior - limiteInferior + 1) + limiteInferior;
    }



    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel direccionLabel;
    private javax.swing.JLabel direccionLabel1;
    private javax.swing.JTextField direccionTxt;
    private javax.swing.JLabel dniLabel;
    private javax.swing.JTextField dniTxt;
    private javax.swing.JTextField emailTxt;
    private javax.swing.JLabel fechaNacimientoLabel;
    private javax.swing.JFormattedTextField fechaNacimientoTxt;
    private javax.swing.JButton jButton1;
    private javax.swing.JComboBox<String> jComboSexo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JLabel nombreLabel;
    private javax.swing.JTextField nombreTxt;
    private javax.swing.JLabel numeroDeLicenciaLabel;
    private javax.swing.JTextField numeroDeLicenciaTxt;
    private javax.swing.JLabel numeroTelefonoLabel;
    private javax.swing.JTextField numeroTelefonoTxt;
    private javax.swing.JLabel sexoLabel;
    private javax.swing.JLabel tipoDeLicenciaLabel;
    private javax.swing.JTextField tipoDeLicenciaTxt;
    // End of variables declaration//GEN-END:variables
}
