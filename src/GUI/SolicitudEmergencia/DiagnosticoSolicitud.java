/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package GUI.SolicitudEmergencia;

import Excepciones.AltaException;
import Excepciones.InexistenteException;
import emergencia.Empresa;
import gestionAfiliado.Afiliado;
import gestionAfiliado.Meses;
import gestionAsistenciaMedica.SolicitudAsistenciaMedica;
import gestionAsistenciaMedica.TipoSolicitud;
import gestionEmpleados.Chofer;
import gestionEmpleados.Doctor;
import gestionEmpleados.Empleado;
import gestionEmpleados.Enfermero;
import gestionMoviles.Movil;
import java.awt.Color;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

/**
 *
 * @author Valentin Roldan
 */
public class DiagnosticoSolicitud extends javax.swing.JDialog {
    
     private final Empresa gestionEmpresa;
     private String diagnostico;

    
    public DiagnosticoSolicitud(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        gestionEmpresa=Empresa.intancia();
        

    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        etqTitulo = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jSeparator7 = new javax.swing.JSeparator();
        diagnosticoTxt = new javax.swing.JTextField();
        lblMedico = new javax.swing.JLabel();
        btnAgregarDiagnostico = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(204, 204, 204));

        etqTitulo.setFont(new java.awt.Font("Swis721 Ex BT", 1, 24)); // NOI18N
        etqTitulo.setText("RegistrarDiagnostico");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(112, 112, 112)
                .addComponent(etqTitulo)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(etqTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 20, 700, 80));

        jPanel4.setBackground(new java.awt.Color(0, 204, 204));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/icono versiofinal.png"))); // NOI18N

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 240, 630));

        jPanel5.setBackground(new java.awt.Color(0, 204, 204));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(920, 0, -1, 630));

        jSeparator7.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator7, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 300, 410, 20));

        diagnosticoTxt.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        diagnosticoTxt.setForeground(new java.awt.Color(204, 204, 204));
        diagnosticoTxt.setBorder(null);
        diagnosticoTxt.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                diagnosticoTxtMousePressed(evt);
            }
        });
        diagnosticoTxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                diagnosticoTxtActionPerformed(evt);
            }
        });
        jPanel1.add(diagnosticoTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 270, 420, 30));

        lblMedico.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        lblMedico.setText("Diagnostico");
        jPanel1.add(lblMedico, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 280, -1, -1));

        btnAgregarDiagnostico.setBackground(new java.awt.Color(0, 204, 204));
        btnAgregarDiagnostico.setText("Agregar Diagostico");
        btnAgregarDiagnostico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarDiagnosticoActionPerformed(evt);
            }
        });
        jPanel1.add(btnAgregarDiagnostico, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 560, 172, 41));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 7, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnAgregarDiagnosticoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarDiagnosticoActionPerformed
        diagnostico= diagnosticoTxt.getText();
        
        dispose();
           
           
    }//GEN-LAST:event_btnAgregarDiagnosticoActionPerformed

    private void diagnosticoTxtMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_diagnosticoTxtMousePressed
       
    }//GEN-LAST:event_diagnosticoTxtMousePressed

    private void diagnosticoTxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_diagnosticoTxtActionPerformed
        
    }//GEN-LAST:event_diagnosticoTxtActionPerformed
     

    public String mensajeDiagnostico(){
        
        return diagnostico;
    }
       
    
    
    
   


        
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregarDiagnostico;
    private javax.swing.JTextField diagnosticoTxt;
    private javax.swing.JLabel etqTitulo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JLabel lblMedico;
    // End of variables declaration//GEN-END:variables
}



//        AltaFamiliarAfiliado ventanaAgregarFamiliar = new AltaFamiliarAfiliado(new javax.swing.JFrame(),true);
//        ventanaAgregarFamiliar.setVisible(true);
//        familiarNuevo = ventanaAgregarFamiliar.getNuevoFamiliar(); //se agrega el familiar
//        familiaresAgregados.add(familiarNuevo); // se agrega provisoriamente a una tabla
//        panelFamiliares.desplegarTabla(familiaresAgregados);
//        JOptionPane.showMessageDialog(null, "Se agregó el familiar", "EXITO", JOptionPane.INFORMATION_MESSAGE);