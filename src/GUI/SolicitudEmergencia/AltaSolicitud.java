/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package GUI.SolicitudEmergencia;

import Excepciones.AltaException;
import Excepciones.InexistenteException;
import emergencia.Empresa;
import gestionAfiliado.Afiliado;

import gestionAsistenciaMedica.SolicitudAsistenciaMedica;
import gestionAsistenciaMedica.TipoSolicitud;
import gestionEmpleados.Chofer;
import gestionEmpleados.Doctor;
import gestionEmpleados.Empleado;
import gestionEmpleados.Enfermero;
import gestionMoviles.Movil;
import java.awt.Color;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

/**
 *
 * @author Valentin Roldan
 */
public class AltaSolicitud extends javax.swing.JDialog {
    
     private final Empresa gestionEmpresa;

    
    public AltaSolicitud(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        gestionEmpresa=Empresa.intancia();
        cargarComboBox();
       

    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        etqTitulo = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        afiliadoTxt = new javax.swing.JTextField();
        jSeparator4 = new javax.swing.JSeparator();
        lblTipoSolicitud = new javax.swing.JLabel();
        lblAfiliado = new javax.swing.JLabel();
        lblAmbulancia = new javax.swing.JLabel();
        jSeparator7 = new javax.swing.JSeparator();
        medicoTxt = new javax.swing.JTextField();
        ambulanciaTxt = new javax.swing.JTextField();
        jSeparator8 = new javax.swing.JSeparator();
        lblMedico = new javax.swing.JLabel();
        lblChofer = new javax.swing.JLabel();
        lblEnfermero = new javax.swing.JLabel();
        enfermeroTxt = new javax.swing.JTextField();
        jSeparator9 = new javax.swing.JSeparator();
        btnAgregar = new javax.swing.JButton();
        choferTxt = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        lblBuscarAmbulancia = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        lblBuscarMedico = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        lblBuscarEnfermero = new javax.swing.JLabel();
        lblBuscarAfiliado1 = new javax.swing.JLabel();
        jComboTipoDeSolicitud = new javax.swing.JComboBox<>();
        jPanel8 = new javax.swing.JPanel();
        lblBuscarChofer = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(204, 204, 204));

        etqTitulo.setFont(new java.awt.Font("Swis721 Ex BT", 1, 24)); // NOI18N
        etqTitulo.setText("Registro Solicitud de Asistencia");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(112, 112, 112)
                .addComponent(etqTitulo)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(etqTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 20, 700, 80));

        jPanel4.setBackground(new java.awt.Color(0, 204, 204));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/icono versiofinal.png"))); // NOI18N

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 240, 630));

        jPanel5.setBackground(new java.awt.Color(0, 204, 204));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(920, 0, -1, 630));

        jSeparator1.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 180, 410, 20));

        jSeparator2.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 230, 410, 10));

        afiliadoTxt.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        afiliadoTxt.setForeground(new java.awt.Color(51, 51, 51));
        afiliadoTxt.setText("Ingrese Numero De Afiliado");
        afiliadoTxt.setBorder(null);
        afiliadoTxt.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                afiliadoTxtMousePressed(evt);
            }
        });
        afiliadoTxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                afiliadoTxtActionPerformed(evt);
            }
        });
        jPanel1.add(afiliadoTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 200, 420, 30));

        jSeparator4.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 280, 410, 20));

        lblTipoSolicitud.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        lblTipoSolicitud.setText("Tipo de Solicitud");
        jPanel1.add(lblTipoSolicitud, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 160, -1, -1));

        lblAfiliado.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        lblAfiliado.setText("Afiliado");
        jPanel1.add(lblAfiliado, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 210, -1, -1));

        lblAmbulancia.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        lblAmbulancia.setText("Ambulancia");
        jPanel1.add(lblAmbulancia, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 260, -1, -1));

        jSeparator7.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator7, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 380, 410, 20));

        medicoTxt.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        medicoTxt.setForeground(new java.awt.Color(51, 51, 51));
        medicoTxt.setText("Ingrese Numero De Agente Del Medico");
        medicoTxt.setBorder(null);
        medicoTxt.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                medicoTxtMousePressed(evt);
            }
        });
        jPanel1.add(medicoTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 350, 420, 30));

        ambulanciaTxt.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        ambulanciaTxt.setForeground(new java.awt.Color(51, 51, 51));
        ambulanciaTxt.setText("Ingrese Numero De Ambulancia");
        ambulanciaTxt.setBorder(null);
        ambulanciaTxt.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                ambulanciaTxtMousePressed(evt);
            }
        });
        ambulanciaTxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ambulanciaTxtActionPerformed(evt);
            }
        });
        jPanel1.add(ambulanciaTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 250, 420, 30));

        jSeparator8.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator8, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 330, 410, 20));

        lblMedico.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        lblMedico.setText("Medico");
        jPanel1.add(lblMedico, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 360, -1, -1));

        lblChofer.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        lblChofer.setText("Chofer");
        jPanel1.add(lblChofer, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 310, -1, -1));

        lblEnfermero.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        lblEnfermero.setText("Enfermero");
        jPanel1.add(lblEnfermero, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 410, -1, -1));

        enfermeroTxt.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        enfermeroTxt.setForeground(new java.awt.Color(51, 51, 51));
        enfermeroTxt.setText("Ingrese Numero De Agente Del Enfermero");
        enfermeroTxt.setBorder(null);
        enfermeroTxt.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                enfermeroTxtMousePressed(evt);
            }
        });
        enfermeroTxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enfermeroTxtActionPerformed(evt);
            }
        });
        jPanel1.add(enfermeroTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 400, 420, 30));

        jSeparator9.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator9, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 430, 410, 20));

        btnAgregar.setBackground(new java.awt.Color(0, 204, 204));
        btnAgregar.setText("Agregar");
        btnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarActionPerformed(evt);
            }
        });
        jPanel1.add(btnAgregar, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 560, 172, 41));

        choferTxt.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        choferTxt.setForeground(new java.awt.Color(51, 51, 51));
        choferTxt.setText("Ingrese Numero De Agente Del Chofer");
        choferTxt.setBorder(null);
        choferTxt.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                choferTxtMousePressed(evt);
            }
        });
        jPanel1.add(choferTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 300, 420, 30));

        lblBuscarAmbulancia.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/search_locate_find_icon-icons.com_67287.png"))); // NOI18N
        lblBuscarAmbulancia.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                lblBuscarAmbulanciaMousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addComponent(lblBuscarAmbulancia)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(lblBuscarAmbulancia, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(830, 250, 30, 30));

        lblBuscarMedico.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/search_locate_find_icon-icons.com_67287.png"))); // NOI18N
        lblBuscarMedico.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                lblBuscarMedicoMousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addComponent(lblBuscarMedico)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(lblBuscarMedico, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel1.add(jPanel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(830, 350, 30, 30));

        lblBuscarEnfermero.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/search_locate_find_icon-icons.com_67287.png"))); // NOI18N
        lblBuscarEnfermero.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                lblBuscarEnfermeroMousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(lblBuscarEnfermero))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(lblBuscarEnfermero, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel1.add(jPanel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(830, 400, -1, -1));

        lblBuscarAfiliado1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/search_locate_find_icon-icons.com_67287.png"))); // NOI18N
        lblBuscarAfiliado1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                lblBuscarAfiliado1MousePressed(evt);
            }
        });
        jPanel1.add(lblBuscarAfiliado1, new org.netbeans.lib.awtextra.AbsoluteConstraints(830, 200, -1, 30));

        jComboTipoDeSolicitud.setForeground(new java.awt.Color(204, 204, 204));
        jComboTipoDeSolicitud.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Urgente", "No urgente", " " }));
        jComboTipoDeSolicitud.setAutoscrolls(true);
        jComboTipoDeSolicitud.setBorder(null);
        jComboTipoDeSolicitud.setDebugGraphicsOptions(javax.swing.DebugGraphics.NONE_OPTION);
        jComboTipoDeSolicitud.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboTipoDeSolicitudActionPerformed(evt);
            }
        });
        jPanel1.add(jComboTipoDeSolicitud, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 150, 410, 30));

        lblBuscarChofer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/search_locate_find_icon-icons.com_67287.png"))); // NOI18N
        lblBuscarChofer.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                lblBuscarChoferMousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblBuscarChofer, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(lblBuscarChofer, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel1.add(jPanel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(830, 300, -1, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void afiliadoTxtMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_afiliadoTxtMousePressed
        if (afiliadoTxt.getText().equals("Ingrese Numero De Afiliado")) {
            afiliadoTxt.setText("");
            afiliadoTxt.setForeground(Color.black);
        }
         

        if (ambulanciaTxt.getText().isEmpty()) {
            ambulanciaTxt.setText("Ingrese Numero De Ambulancia");
            ambulanciaTxt.setForeground(Color.gray);
        }
        if (medicoTxt.getText().isEmpty()) {
            medicoTxt.setText("Ingrese Numero De Agente Del Medico");
            medicoTxt.setForeground(Color.gray);
        }
        if (enfermeroTxt.getText().isEmpty()) {
            enfermeroTxt.setText("Ingrese Numero De Agente Del Enfermero");
            enfermeroTxt.setForeground(Color.gray);
        }
        if (choferTxt.getText().isEmpty()) {
            choferTxt.setText("Ingrese Numero De Agente Del Chofer");
            choferTxt.setForeground(Color.gray);
        }
              
        
    }//GEN-LAST:event_afiliadoTxtMousePressed

    private void medicoTxtMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_medicoTxtMousePressed
        if (medicoTxt.getText().equals("Ingrese Numero De Agente Del Medico")) {
            medicoTxt.setText("");
            medicoTxt.setForeground(Color.black);
        }
        
       if (ambulanciaTxt.getText().isEmpty()) {
            ambulanciaTxt.setText("Ingrese Numero De Ambulancia");
            ambulanciaTxt.setForeground(Color.gray);
        }
       
        if (enfermeroTxt.getText().isEmpty()) {
            enfermeroTxt.setText("Ingrese Numero De Agente Del Enfermero");
            enfermeroTxt.setForeground(Color.gray);
        }
        if (choferTxt.getText().isEmpty()) {
            choferTxt.setText("Ingrese Numero De Agente Del Chofer");
            choferTxt.setForeground(Color.gray);
        }
        if (afiliadoTxt.getText().isEmpty()) {
            afiliadoTxt.setText("Ingrese Numero De Afiliado");
            afiliadoTxt.setForeground(Color.gray);
        }
              
        
        
        
    }//GEN-LAST:event_medicoTxtMousePressed

    private void ambulanciaTxtMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ambulanciaTxtMousePressed
        if (ambulanciaTxt.getText().equals("Ingrese Numero De Ambulancia")) {
            ambulanciaTxt.setText("");
            ambulanciaTxt.setForeground(Color.black);
        }
        if (medicoTxt.getText().isEmpty()) {
            medicoTxt.setText("Ingrese Numero De Agente Del Medico");
            medicoTxt.setForeground(Color.gray);
        }
       
        if (enfermeroTxt.getText().isEmpty()) {
            enfermeroTxt.setText("Ingrese Numero De Agente Del Enfermero");
            enfermeroTxt.setForeground(Color.gray);
        }
        if (choferTxt.getText().isEmpty()) {
            choferTxt.setText("Ingrese Numero De Agente Del Chofer");
            choferTxt.setForeground(Color.gray);
        }
        if (afiliadoTxt.getText().isEmpty()) {
            afiliadoTxt.setText("Ingrese Numero De Afiliado");
            afiliadoTxt.setForeground(Color.gray);
        }
        
       
       
    }//GEN-LAST:event_ambulanciaTxtMousePressed

    private void btnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarActionPerformed

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/M/yyyy");
        String diaActual = String.valueOf(LocalDate.now().getDayOfMonth());
        String mesActual = String.valueOf(LocalDate.now().getMonthValue());
        String yearActual = String.valueOf(LocalDate.now().getYear());
        String fechaAfiliacionString = diaActual + "/" + mesActual + "/" + yearActual;
        System.out.println(fechaAfiliacionString);
        try {
            
            
            LocalDate fechaSolicitud = LocalDate.parse(fechaAfiliacionString, formatter);
            
            Afiliado afiliado= gestionEmpresa.getGestionAfiliados().buscar(Integer.parseInt(afiliadoTxt.getText()));
           
            Movil ambulancia= gestionEmpresa.getGestionMoviles().buscar(ambulanciaTxt.getText()); 
        
            Empleado doctor= gestionEmpresa.getGestionEmpleados().buscar(Integer.parseInt(medicoTxt.getText()));
           
            Empleado enfermero= gestionEmpresa.getGestionEmpleados().buscar(Integer.parseInt(enfermeroTxt.getText()));
  
          
            Empleado chofer=  gestionEmpresa.getGestionEmpleados().buscar(Integer.parseInt(choferTxt.getText()));

            
            SolicitudAsistenciaMedica nuevaSolicitud= new SolicitudAsistenciaMedica((TipoSolicitud) jComboTipoDeSolicitud.getSelectedItem(),afiliado,fechaSolicitud,ambulancia,(Doctor) doctor,(Enfermero) enfermero, (Chofer)chofer);
            gestionEmpresa.getGestionAsistenciasMedicas().agregarSolicitudAsistenciaMedica(nuevaSolicitud);
            this.dispose();
         } catch (InexistenteException | AltaException ex) {
             JOptionPane.showMessageDialog(this, ex.getMessage());
         }
        
    }//GEN-LAST:event_btnAgregarActionPerformed

    private void enfermeroTxtMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_enfermeroTxtMousePressed
        if (enfermeroTxt.getText().equals("Ingrese Numero De Agente Del Enfermero")) {
            enfermeroTxt.setText("");
            enfermeroTxt.setForeground(Color.black);
        }
        
        if (ambulanciaTxt.getText().isEmpty()) {
            ambulanciaTxt.setText("Ingrese Numero De Ambulancia");
            ambulanciaTxt.setForeground(Color.gray);
        }
       
        if (choferTxt.getText().isEmpty()) {
            choferTxt.setText("Ingrese Numero De Agente Del Chofer");
            choferTxt.setForeground(Color.gray);
        }
        if (afiliadoTxt.getText().isEmpty()) {
            afiliadoTxt.setText("Ingrese Numero De Afiliado");
            afiliadoTxt.setForeground(Color.gray);
        }
        if (medicoTxt.getText().isEmpty()) {
            medicoTxt.setText("Ingrese Numero De Agente Del Medico");
            medicoTxt.setForeground(Color.gray);
        }

    }//GEN-LAST:event_enfermeroTxtMousePressed

    private void choferTxtMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_choferTxtMousePressed
        if (choferTxt.getText().equals("Ingrese Numero De Agente Del Chofer")) {
            choferTxt.setText("");
            choferTxt.setForeground(Color.black);
        }
        
        if (enfermeroTxt.getText().isEmpty()) {
            enfermeroTxt.setText("Ingrese Numero De Agente Del Enfermero");
            enfermeroTxt.setForeground(Color.gray);
        }
        if (ambulanciaTxt.getText().isEmpty()) {
            ambulanciaTxt.setText("Ingrese Numero De Ambulancia");
            ambulanciaTxt.setForeground(Color.gray);
        }
        
        if (afiliadoTxt.getText().isEmpty()) {
            afiliadoTxt.setText("Ingrese Numero De Afiliado");
            afiliadoTxt.setForeground(Color.gray);
        }
        if (medicoTxt.getText().isEmpty()) {
            medicoTxt.setText("Ingrese Numero De Agente Del Medico");
            medicoTxt.setForeground(Color.gray);
        }
    }//GEN-LAST:event_choferTxtMousePressed

    private void ambulanciaTxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ambulanciaTxtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ambulanciaTxtActionPerformed

    private void lblBuscarAmbulanciaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblBuscarAmbulanciaMousePressed
        BuscarAmbulancia buscar = new  BuscarAmbulancia(new javax.swing.JFrame(), true, this.ambulanciaTxt);
        buscar.setVisible(true);

    }//GEN-LAST:event_lblBuscarAmbulanciaMousePressed

    private void lblBuscarChoferMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblBuscarChoferMousePressed
        BuscarChofer buscar = new  BuscarChofer(new javax.swing.JFrame(), true, this.choferTxt);
        buscar.setVisible(true);
    }//GEN-LAST:event_lblBuscarChoferMousePressed

    private void lblBuscarMedicoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblBuscarMedicoMousePressed
        BuscarMedico buscar = new  BuscarMedico(new javax.swing.JFrame(), true, this.medicoTxt);
        buscar.setVisible(true);
    }//GEN-LAST:event_lblBuscarMedicoMousePressed

    private void lblBuscarEnfermeroMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblBuscarEnfermeroMousePressed
        BuscarEnfermero buscar = new  BuscarEnfermero(new javax.swing.JFrame(), true, this.enfermeroTxt);
        buscar.setVisible(true);
    }//GEN-LAST:event_lblBuscarEnfermeroMousePressed

    private void lblBuscarAfiliado1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblBuscarAfiliado1MousePressed
        BuscarAfiliado buscar = new  BuscarAfiliado(new javax.swing.JFrame(), true, this.afiliadoTxt);
        buscar.setVisible(true);
    }//GEN-LAST:event_lblBuscarAfiliado1MousePressed

    private void jComboTipoDeSolicitudActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboTipoDeSolicitudActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboTipoDeSolicitudActionPerformed

    private void afiliadoTxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_afiliadoTxtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_afiliadoTxtActionPerformed

    private void enfermeroTxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enfermeroTxtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_enfermeroTxtActionPerformed
     

    private void cargarComboBox(){
        JComboBox<TipoSolicitud> miComboBox = new JComboBox<>();
        jComboTipoDeSolicitud.setModel(new DefaultComboBoxModel(TipoSolicitud.values()));   
    }

    
    
    
   


        
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField afiliadoTxt;
    private javax.swing.JTextField ambulanciaTxt;
    private javax.swing.JButton btnAgregar;
    private javax.swing.JTextField choferTxt;
    private javax.swing.JTextField enfermeroTxt;
    private javax.swing.JLabel etqTitulo;
    private javax.swing.JComboBox<String> jComboTipoDeSolicitud;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JLabel lblAfiliado;
    private javax.swing.JLabel lblAmbulancia;
    private javax.swing.JLabel lblBuscarAfiliado1;
    private javax.swing.JLabel lblBuscarAmbulancia;
    private javax.swing.JLabel lblBuscarChofer;
    private javax.swing.JLabel lblBuscarEnfermero;
    private javax.swing.JLabel lblBuscarMedico;
    private javax.swing.JLabel lblChofer;
    private javax.swing.JLabel lblEnfermero;
    private javax.swing.JLabel lblMedico;
    private javax.swing.JLabel lblTipoSolicitud;
    private javax.swing.JTextField medicoTxt;
    // End of variables declaration//GEN-END:variables
}



//        AltaFamiliarAfiliado ventanaAgregarFamiliar = new AltaFamiliarAfiliado(new javax.swing.JFrame(),true);
//        ventanaAgregarFamiliar.setVisible(true);
//        familiarNuevo = ventanaAgregarFamiliar.getNuevoFamiliar(); //se agrega el familiar
//        familiaresAgregados.add(familiarNuevo); // se agrega provisoriamente a una tabla
//        panelFamiliares.desplegarTabla(familiaresAgregados);
//        JOptionPane.showMessageDialog(null, "Se agregó el familiar", "EXITO", JOptionPane.INFORMATION_MESSAGE);