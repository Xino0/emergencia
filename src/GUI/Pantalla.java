/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package GUI;

import Excepciones.InexistenteException;
import GUI.Empleado.PantallaEmpleado;
import GUI.SolicitudEmergencia.AltaSolicitud;
import GUI.SolicitudEmergencia.DiagnosticoSolicitud;
import emergencia.Empresa;
import GUI_Afiliado.PanelAfiliado;
import GUI_automoviles.PantallaMoviles;
import gestionAsistenciaMedica.SolicitudAsistenciaMedica;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import javax.swing.JOptionPane;
import modelos.Tablas.ModeloTablaEmpleados;
import modelos.Tablas.ModeloTablaSolicitudes;


/**
 *
 * @author david
 */
public class Pantalla extends javax.swing.JFrame {
    private final Empresa gestionEmpresa;
    private final PanelAfiliado panelAfiliados = new PanelAfiliado();
    //private final PantallaMoviles panelMoviles = new PantallaMoviles();
    
    public Pantalla() {
        
        initComponents();
        jPanel1.add(panelAfiliados, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1510, 880));
        gestionEmpresa= Empresa.intancia();
        
        panelAfiliados.setVisible(false);
        setModeloTabla();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel4 = new javax.swing.JPanel();
        jSlider1 = new javax.swing.JSlider();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        btnGestionEmpleados = new javax.swing.JButton();
        btnGestionAfiliados = new javax.swing.JButton();
        btnDiagnostico = new javax.swing.JButton();
        btnSolicitud = new javax.swing.JButton();
        btnAutomoviles = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        etiquetaMenu = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableSolicitudes = new javax.swing.JTable();
        etiquetaMenu1 = new javax.swing.JLabel();

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setBackground(new java.awt.Color(0, 204, 204));

        btnGestionEmpleados.setText("Gestion Empleados");
        btnGestionEmpleados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGestionEmpleadosActionPerformed(evt);
            }
        });

        btnGestionAfiliados.setText("Gestion Afiliados");
        btnGestionAfiliados.setBorder(null);
        btnGestionAfiliados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGestionAfiliadosActionPerformed(evt);
            }
        });

        btnDiagnostico.setText("Registrar Diagnostico");
        btnDiagnostico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDiagnosticoActionPerformed(evt);
            }
        });

        btnSolicitud.setText("Nueva Solicitud");
        btnSolicitud.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSolicitudActionPerformed(evt);
            }
        });

        btnAutomoviles.setText("Automoviles");
        btnAutomoviles.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAutomovilesActionPerformed(evt);
            }
        });

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/icono versiofinal.png"))); // NOI18N
        jLabel1.setText("jLabel1");
        jLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel1MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(btnGestionAfiliados, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                            .addGap(36, 36, 36)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(btnGestionEmpleados, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnAutomoviles, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnDiagnostico, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnSolicitud, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(37, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addComponent(btnGestionAfiliados, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnGestionEmpleados, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnSolicitud, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnDiagnostico, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnAutomoviles, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(425, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 240, 880));

        jPanel3.setBackground(new java.awt.Color(204, 204, 204));

        etiquetaMenu.setFont(new java.awt.Font("Swis721 Ex BT", 1, 24)); // NOI18N
        etiquetaMenu.setText("Menu Emergencia");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(518, Short.MAX_VALUE)
                .addComponent(etiquetaMenu, javax.swing.GroupLayout.PREFERRED_SIZE, 333, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(469, 469, 469))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(etiquetaMenu, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 30, 1320, 80));

        jTableSolicitudes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTableSolicitudes);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 140, 1270, 740));

        etiquetaMenu1.setFont(new java.awt.Font("Swis721 Ex BT", 1, 24)); // NOI18N
        etiquetaMenu1.setText("Lista de solicitudes");
        jPanel1.add(etiquetaMenu1, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 110, 290, 30));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnGestionEmpleadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGestionEmpleadosActionPerformed
        PantallaEmpleado pantallaEmpleado= new PantallaEmpleado(this,true);
        this.setVisible(false);
        this.dispose();
        pantallaEmpleado.setVisible(true);
    }//GEN-LAST:event_btnGestionEmpleadosActionPerformed

    private void btnSolicitudActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSolicitudActionPerformed
        jScrollPane1.setVisible(true);
        jTableSolicitudes.setVisible(true);
        etiquetaMenu1.setVisible(true);
        etiquetaMenu.setText("Menu Emergencia");
        panelAfiliados.setVisible(false);
        
        AltaSolicitud nuevaSolicitud= new AltaSolicitud(this,true); 
        nuevaSolicitud.setVisible(true);
        setModeloTabla();
        
    }//GEN-LAST:event_btnSolicitudActionPerformed

    private void btnGestionAfiliadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGestionAfiliadosActionPerformed
        jScrollPane1.setVisible(false);
        jTableSolicitudes.setVisible(false);
        etiquetaMenu1.setVisible(false);
        panelAfiliados.setVisible(true);
        panelAfiliados.desplegarTabla();

        etiquetaMenu.setText("Gestion Afiliados");
    }//GEN-LAST:event_btnGestionAfiliadosActionPerformed

    private void jLabel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseClicked
        panelAfiliados.setVisible(false);
        etiquetaMenu.setText("Menu Emergencia");
    }//GEN-LAST:event_jLabel1MouseClicked

    private void btnAutomovilesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAutomovilesActionPerformed
        PantallaMoviles pantallaMoviles = new PantallaMoviles();
        //this.setVisible(false);
        //this.dispose();
        pantallaMoviles.setVisible(true);
    }//GEN-LAST:event_btnAutomovilesActionPerformed

    private void btnDiagnosticoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDiagnosticoActionPerformed
        DiagnosticoSolicitud ventanaDiagnostico= new DiagnosticoSolicitud(this,true);
        ventanaDiagnostico.setVisible(true);
        String mensajeDiagnostico=ventanaDiagnostico.mensajeDiagnostico();
        if(jTableSolicitudes.getSelectedRow() == -1){
            JOptionPane.showMessageDialog(null, "Debe seleccionar una Solicitud", "Error",JOptionPane.WARNING_MESSAGE);     
        }
        else{
            SolicitudAsistenciaMedica Original = gestionEmpresa.getGestionAsistenciasMedicas().getSolicitudAsistenciaMedica().get(jTableSolicitudes.getSelectedRow());
            SolicitudAsistenciaMedica modificar= Original;
            modificar.getAmbulancia().setDisponibilidad(true);
            modificar.getChofer().setDisponibilidad(true);
            modificar.getDoctor().setDisponibilidad(true);
            modificar.getEnfermero().setDisponibilidad(true);
            modificar.setDiagnositco(mensajeDiagnostico);
            try {
                gestionEmpresa.getGestionAsistenciasMedicas().modificarSolicitudAsistenciaMedica(Original, modificar);
            } catch (InexistenteException ex) {
               
            }
            setModeloTabla();
        }
        
     
    }//GEN-LAST:event_btnDiagnosticoActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Pantalla.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Pantalla.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Pantalla.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Pantalla.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Pantalla().setVisible(true);
                
            }
        });
    }
    
    private void setModeloTabla(){
      ModeloTablaSolicitudes modelo= new  ModeloTablaSolicitudes();
      modelo.setSolicitudes(gestionEmpresa.getGestionAsistenciasMedicas().getSolicitudAsistenciaMedica());
      
       this.jTableSolicitudes.setModel( modelo);
       
   }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAutomoviles;
    private javax.swing.JButton btnDiagnostico;
    private javax.swing.JButton btnGestionAfiliados;
    private javax.swing.JButton btnGestionEmpleados;
    private javax.swing.JButton btnSolicitud;
    private javax.swing.JLabel etiquetaMenu;
    private javax.swing.JLabel etiquetaMenu1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSlider jSlider1;
    private javax.swing.JTable jTableSolicitudes;
    // End of variables declaration//GEN-END:variables
}
