/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package GUI_Afiliado;
import emergencia.Empresa;
import gestionAfiliado.Familiar;
import java.util.ArrayList;
import java.util.List;



/**
 *
 * @author Valentin Roldan
 */
public class DetallesFamiliar extends javax.swing.JDialog {
    private ModeloTabla modeloTabla = new ModeloTabla();
    private final Empresa afiliados;
    int i = 1;
    public DetallesFamiliar(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        afiliados = Empresa.intancia();
        initComponents();
        configuracionModeloTabla();
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        etqTitulo = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        labelNombre = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        ingrNombre = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        ingrDni = new javax.swing.JLabel();
        ingrFn = new javax.swing.JLabel();
        ingrGenero = new javax.swing.JLabel();
        ingrNtel = new javax.swing.JLabel();
        ingrEmail = new javax.swing.JLabel();
        ingrDireccion = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(204, 204, 204));

        etqTitulo.setFont(new java.awt.Font("Swis721 Ex BT", 1, 24)); // NOI18N
        etqTitulo.setText("Datos Familiar");

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/salud-y-cuidado.png"))); // NOI18N

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/familia.png"))); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 282, Short.MAX_VALUE)
                .addComponent(etqTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 430, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(154, 154, 154))
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                    .addContainerGap(856, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(16, 16, 16)))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(etqTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );

        jPanel1.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 30, 990, 80));

        jPanel4.setBackground(new java.awt.Color(51, 255, 204));

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 60, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 870, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 60, 870));

        jPanel5.setBackground(new java.awt.Color(51, 255, 204));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 60, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 870, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(1030, 0, 60, 870));

        labelNombre.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        labelNombre.setText("Nombre:");
        jPanel1.add(labelNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 160, -1, -1));

        jLabel4.setFont(new java.awt.Font("Segoe UI Black", 1, 24)); // NOI18N
        jLabel4.setText("DNI: ");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 230, -1, -1));

        ingrNombre.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        ingrNombre.setText("jLabel3");
        jPanel1.add(ingrNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 160, -1, -1));

        jLabel5.setFont(new java.awt.Font("Segoe UI Black", 1, 24)); // NOI18N
        jLabel5.setText("Fecha de nacimiento:");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 290, -1, -1));

        jLabel6.setFont(new java.awt.Font("Segoe UI Black", 1, 24)); // NOI18N
        jLabel6.setText("Genero:");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 350, -1, -1));

        jLabel7.setFont(new java.awt.Font("Segoe UI Black", 1, 24)); // NOI18N
        jLabel7.setText("Nro de telefono:");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 410, -1, -1));

        jLabel8.setFont(new java.awt.Font("Segoe UI Black", 1, 24)); // NOI18N
        jLabel8.setText("Direccion:");
        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 550, -1, -1));

        jLabel9.setFont(new java.awt.Font("Segoe UI Black", 1, 24)); // NOI18N
        jLabel9.setText("Email:");
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 480, -1, -1));

        ingrDni.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        ingrDni.setText("jLabel10");
        jPanel1.add(ingrDni, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 230, -1, -1));

        ingrFn.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        ingrFn.setText("jLabel12");
        jPanel1.add(ingrFn, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 290, -1, -1));

        ingrGenero.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        ingrGenero.setText("jLabel13");
        jPanel1.add(ingrGenero, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 350, -1, -1));

        ingrNtel.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        ingrNtel.setText("jLabel14");
        jPanel1.add(ingrNtel, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 410, -1, -1));

        ingrEmail.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        ingrEmail.setText("jLabel15");
        jPanel1.add(ingrEmail, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 480, -1, -1));

        ingrDireccion.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        ingrDireccion.setText("jLabel16");
        jPanel1.add(ingrDireccion, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 550, -1, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 740, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
    

    
    
    private void configuracionModeloTabla(){
        String [] cabecera = {"PARENTESCO" , "NOMBRE" , "DNI", "FECHA DE NACIMIENTO", "GENERO", "TELEFONO", "EMAIL", "DIRECCION"};
        modeloTabla.setColumnIdentifiers(cabecera);        
    }

    
    protected void detallesFamiliar(int s, int n){
        
        Familiar seleccionado = afiliados.getGestionAfiliados().getLista().get(n).getFamiliares().get(s);
        ingrNombre.setText(seleccionado.getNombre());
        ingrDni.setText(seleccionado.getNombre());
        

        String txtDiaN = String.valueOf(seleccionado.getFechaDeNacimiento().getDayOfMonth());
        String txtMesN = String.valueOf(seleccionado.getFechaDeNacimiento().getMonthValue());
        String txtYearN = String.valueOf(seleccionado.getFechaDeNacimiento().getYear());
        
        String txtFechaN = txtDiaN + "/" + txtMesN + "/" + txtYearN;
        
        ingrFn.setText(txtFechaN);
        ingrGenero.setText(seleccionado.getGenero());
        ingrNtel.setText(seleccionado.getTelefono());
        ingrEmail.setText(seleccionado.getEmail());
        ingrDireccion.setText(seleccionado.getDireccion());

    }
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel etqTitulo;
    private javax.swing.JLabel ingrDireccion;
    private javax.swing.JLabel ingrDni;
    private javax.swing.JLabel ingrEmail;
    private javax.swing.JLabel ingrFn;
    private javax.swing.JLabel ingrGenero;
    private javax.swing.JLabel ingrNombre;
    private javax.swing.JLabel ingrNtel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JLabel labelNombre;
    // End of variables declaration//GEN-END:variables
}
