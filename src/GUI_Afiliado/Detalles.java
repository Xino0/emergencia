/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package GUI_Afiliado;

import emergencia.Empresa;
import gestionAfiliado.Afiliado;
import gestionAfiliado.Familiar;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.swing.JOptionPane;


/**
 *
 * @author Valentin Roldan
 */
public class Detalles extends javax.swing.JDialog {
    private ModeloTabla modeloTabla = new ModeloTabla();
    
    private final Empresa afiliados;
    private List<Familiar> familiaresAgregados;
    int i = 1;
    private int indiceSeleccionado;
    public Detalles(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        afiliados = Empresa.intancia();
        initComponents();
        configuracionModeloTabla();
        familiaresAgregados = new ArrayList();
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        etqTitulo = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaFamiliares = new javax.swing.JTable();
        panelEditar = new javax.swing.JPanel();
        etqEditar = new javax.swing.JLabel();
        panelMas = new javax.swing.JPanel();
        etqMas = new javax.swing.JLabel();
        panelBorrar = new javax.swing.JPanel();
        etqBorrar = new javax.swing.JLabel();
        labelNombre = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        ingrNombre = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        ingrDni = new javax.swing.JLabel();
        ingrFa = new javax.swing.JLabel();
        ingrFn = new javax.swing.JLabel();
        ingrGenero = new javax.swing.JLabel();
        ingrNtel = new javax.swing.JLabel();
        ingrEmail = new javax.swing.JLabel();
        ingrDireccion = new javax.swing.JLabel();
        panelDetallesFa = new javax.swing.JPanel();
        etqDetallesFa = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(204, 204, 204));

        etqTitulo.setFont(new java.awt.Font("Swis721 Ex BT", 1, 24)); // NOI18N
        etqTitulo.setText("Familiares");

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/salud-y-cuidado.png"))); // NOI18N

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/familia.png"))); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 306, Short.MAX_VALUE)
                .addComponent(etqTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 430, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(130, 130, 130))
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                    .addContainerGap(856, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(16, 16, 16)))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(etqTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );

        jPanel1.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 390, 990, 80));

        jPanel4.setBackground(new java.awt.Color(51, 255, 204));

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 60, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 870, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 60, 870));

        jPanel5.setBackground(new java.awt.Color(51, 255, 204));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 60, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 870, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(1030, 0, 60, 870));

        tablaFamiliares.setModel(modeloTabla);
        jScrollPane1.setViewportView(tablaFamiliares);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 480, 970, 330));

        panelEditar.setBackground(new java.awt.Color(51, 255, 204));

        etqEditar.setBackground(new java.awt.Color(51, 255, 204));
        etqEditar.setForeground(new java.awt.Color(0, 255, 153));
        etqEditar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        etqEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/editar.png"))); // NOI18N
        etqEditar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                etqEditarMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                etqEditarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                etqEditarMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                etqEditarMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                etqEditarMouseReleased(evt);
            }
        });

        javax.swing.GroupLayout panelEditarLayout = new javax.swing.GroupLayout(panelEditar);
        panelEditar.setLayout(panelEditarLayout);
        panelEditarLayout.setHorizontalGroup(
            panelEditarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(etqEditar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
        );
        panelEditarLayout.setVerticalGroup(
            panelEditarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(etqEditar, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
        );

        jPanel1.add(panelEditar, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 820, -1, -1));

        panelMas.setBackground(new Color(24, 194, 119));

        etqMas.setBackground(new java.awt.Color(0, 204, 204));
        etqMas.setForeground(new java.awt.Color(0, 204, 204));
        etqMas.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        etqMas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/signoMas.png"))); // NOI18N
        etqMas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                etqMasMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                etqMasMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                etqMasMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                etqMasMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                etqMasMouseReleased(evt);
            }
        });

        javax.swing.GroupLayout panelMasLayout = new javax.swing.GroupLayout(panelMas);
        panelMas.setLayout(panelMasLayout);
        panelMasLayout.setHorizontalGroup(
            panelMasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(etqMas, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
        );
        panelMasLayout.setVerticalGroup(
            panelMasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(etqMas, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
        );

        jPanel1.add(panelMas, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 820, -1, -1));

        panelBorrar.setBackground(new Color(173,15,15));

        etqBorrar.setBackground(new java.awt.Color(0, 204, 204));
        etqBorrar.setForeground(new java.awt.Color(0, 204, 204));
        etqBorrar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        etqBorrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/basura.png"))); // NOI18N
        etqBorrar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                etqBorrarMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                etqBorrarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                etqBorrarMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                etqBorrarMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                etqBorrarMouseReleased(evt);
            }
        });

        javax.swing.GroupLayout panelBorrarLayout = new javax.swing.GroupLayout(panelBorrar);
        panelBorrar.setLayout(panelBorrarLayout);
        panelBorrarLayout.setHorizontalGroup(
            panelBorrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(etqBorrar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
        );
        panelBorrarLayout.setVerticalGroup(
            panelBorrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(etqBorrar, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
        );

        jPanel1.add(panelBorrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(960, 820, -1, -1));

        labelNombre.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        labelNombre.setText("Nombre:");
        jPanel1.add(labelNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 20, -1, -1));

        jLabel4.setFont(new java.awt.Font("Segoe UI Black", 1, 24)); // NOI18N
        jLabel4.setText("DNI: ");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 70, -1, -1));

        ingrNombre.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        ingrNombre.setText("jLabel3");
        jPanel1.add(ingrNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 20, -1, -1));

        jLabel3.setFont(new java.awt.Font("Segoe UI Black", 1, 24)); // NOI18N
        jLabel3.setText("Fecha de afiliacion:");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 120, -1, -1));

        jLabel5.setFont(new java.awt.Font("Segoe UI Black", 1, 24)); // NOI18N
        jLabel5.setText("Fecha de nacimiento:");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 170, -1, -1));

        jLabel6.setFont(new java.awt.Font("Segoe UI Black", 1, 24)); // NOI18N
        jLabel6.setText("Genero:");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 210, -1, -1));

        jLabel7.setFont(new java.awt.Font("Segoe UI Black", 1, 24)); // NOI18N
        jLabel7.setText("Nro de telefono:");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 250, -1, -1));

        jLabel8.setFont(new java.awt.Font("Segoe UI Black", 1, 24)); // NOI18N
        jLabel8.setText("Direccion:");
        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 340, -1, -1));

        jLabel9.setFont(new java.awt.Font("Segoe UI Black", 1, 24)); // NOI18N
        jLabel9.setText("Email:");
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 290, -1, -1));

        ingrDni.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        ingrDni.setText("jLabel10");
        jPanel1.add(ingrDni, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 70, -1, -1));

        ingrFa.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        ingrFa.setText("jLabel11");
        jPanel1.add(ingrFa, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 120, -1, -1));

        ingrFn.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        ingrFn.setText("jLabel12");
        jPanel1.add(ingrFn, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 170, -1, -1));

        ingrGenero.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        ingrGenero.setText("jLabel13");
        jPanel1.add(ingrGenero, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 210, -1, -1));

        ingrNtel.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        ingrNtel.setText("jLabel14");
        jPanel1.add(ingrNtel, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 250, -1, -1));

        ingrEmail.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        ingrEmail.setText("jLabel15");
        jPanel1.add(ingrEmail, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 290, -1, -1));

        ingrDireccion.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        ingrDireccion.setText("jLabel16");
        jPanel1.add(ingrDireccion, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 340, -1, -1));

        panelDetallesFa.setBackground(new Color(165, 62, 202));

        etqDetallesFa.setBackground(new java.awt.Color(0, 204, 204));
        etqDetallesFa.setForeground(new java.awt.Color(0, 204, 204));
        etqDetallesFa.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        etqDetallesFa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/person-info_icon-icons.com_72961 (1).png"))); // NOI18N
        etqDetallesFa.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                etqDetallesFaMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                etqDetallesFaMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                etqDetallesFaMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                etqDetallesFaMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                etqDetallesFaMouseReleased(evt);
            }
        });

        javax.swing.GroupLayout panelDetallesFaLayout = new javax.swing.GroupLayout(panelDetallesFa);
        panelDetallesFa.setLayout(panelDetallesFaLayout);
        panelDetallesFaLayout.setHorizontalGroup(
            panelDetallesFaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(etqDetallesFa, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
        );
        panelDetallesFaLayout.setVerticalGroup(
            panelDetallesFaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(etqDetallesFa, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
        );

        jPanel1.add(panelDetallesFa, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 820, -1, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(1, 1, 1))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void etqEditarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etqEditarMouseClicked
        if(tablaFamiliares.getSelectedRow() == -1){
            JOptionPane.showMessageDialog(null, "Debe seleccionar un familiar", "Error",JOptionPane.WARNING_MESSAGE);              
        }
        else{
            AltaFamiliarAfiliado ventanaModificar = new AltaFamiliarAfiliado(new javax.swing.JFrame(),true);
            
            ventanaModificar.setTitulo("Modificar Familiar del afiliado");
            
            Familiar modificar = familiaresAgregados.get(tablaFamiliares.getSelectedRow());
            ventanaModificar.setParentesco(modificar.getParentesco());
            ventanaModificar.setNombreTxt(modificar.getNombre());
            ventanaModificar.setDniTxt(modificar.getDni());
            ventanaModificar.setNumeroTelefonoTxt(modificar.getTelefono());
            ventanaModificar.setEmailTxt(modificar.getEmail());
            ventanaModificar.setDireccionTxt(modificar.getDireccion());
            ventanaModificar.renombrarBoton();
            
            
            ventanaModificar.setVisible(true);
            
            familiaresAgregados.set(tablaFamiliares.getSelectedRow(), ventanaModificar.getNuevoFamiliar());
            desplegarTabla();
        }
    }//GEN-LAST:event_etqEditarMouseClicked

    private void etqEditarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etqEditarMouseEntered
        panelEditar.setBackground(new Color(0,255,255));
    }//GEN-LAST:event_etqEditarMouseEntered

    private void etqEditarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etqEditarMouseExited
        panelEditar.setBackground(new Color(0,204,204));
    }//GEN-LAST:event_etqEditarMouseExited

    private void etqEditarMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etqEditarMousePressed
        panelEditar.setBackground(new Color(17,141,172));
    }//GEN-LAST:event_etqEditarMousePressed

    private void etqEditarMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etqEditarMouseReleased
        panelEditar.setBackground(new Color(0,255,255));
    }//GEN-LAST:event_etqEditarMouseReleased

    private void etqMasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etqMasMouseClicked
        AltaFamiliarAfiliado ventanaAgregarFamiliar = new AltaFamiliarAfiliado(new javax.swing.JFrame(),true);
        ventanaAgregarFamiliar.setVisible(true);
        if(ventanaAgregarFamiliar.confirmacion()){
           familiaresAgregados.add(ventanaAgregarFamiliar.getNuevoFamiliar());
           desplegarTabla();
        }
    }//GEN-LAST:event_etqMasMouseClicked

    private void etqMasMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etqMasMouseEntered
        panelMas.setBackground(new Color(8,255,0));
    }//GEN-LAST:event_etqMasMouseEntered

    private void etqMasMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etqMasMouseExited
        panelMas.setBackground(new Color(24,194,19));
    }//GEN-LAST:event_etqMasMouseExited

    private void etqMasMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etqMasMousePressed
        panelMas.setBackground(new Color(15,123,12));
    }//GEN-LAST:event_etqMasMousePressed

    private void etqMasMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etqMasMouseReleased
        panelMas.setBackground(new Color(8,255,0));
    }//GEN-LAST:event_etqMasMouseReleased

    private void etqBorrarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etqBorrarMouseClicked
        if(tablaFamiliares.getSelectedRow() == -1){
            JOptionPane.showMessageDialog(null, "Debe seleccionar un familiar", "Error",JOptionPane.WARNING_MESSAGE);              
        }
        else{
            familiaresAgregados.remove(tablaFamiliares.getSelectedRow());
            desplegarTabla();
        }
    }//GEN-LAST:event_etqBorrarMouseClicked

    private void etqBorrarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etqBorrarMouseEntered
        panelBorrar.setBackground(new Color(255,0,0));
    }//GEN-LAST:event_etqBorrarMouseEntered

    private void etqBorrarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etqBorrarMouseExited
        panelBorrar.setBackground(new Color(173,15,15));
    }//GEN-LAST:event_etqBorrarMouseExited

    private void etqBorrarMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etqBorrarMousePressed
        panelBorrar.setBackground(new Color(142,23,23));
    }//GEN-LAST:event_etqBorrarMousePressed

    private void etqBorrarMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etqBorrarMouseReleased
        panelBorrar.setBackground(new Color(255,0,0));
    }//GEN-LAST:event_etqBorrarMouseReleased

    private void etqDetallesFaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etqDetallesFaMouseClicked
        DetallesFamiliar detallesFa = new DetallesFamiliar(new javax.swing.JFrame(),true);
        detallesFa.detallesFamiliar(tablaFamiliares.getSelectedRow(), indiceSeleccionado);
        detallesFa.setVisible(true);
    }//GEN-LAST:event_etqDetallesFaMouseClicked

    private void etqDetallesFaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etqDetallesFaMouseEntered
        panelDetallesFa.setBackground(new Color(200,50,255));
    }//GEN-LAST:event_etqDetallesFaMouseEntered

    private void etqDetallesFaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etqDetallesFaMouseExited
        panelDetallesFa.setBackground(new Color(165,62,202));
    }//GEN-LAST:event_etqDetallesFaMouseExited

    private void etqDetallesFaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etqDetallesFaMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_etqDetallesFaMousePressed

    private void etqDetallesFaMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etqDetallesFaMouseReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_etqDetallesFaMouseReleased
    

    

    private Integer GeneradorNumeroAgente() {
        Integer  limiteInferior = 1000;
        Integer  limiteSuperior = 9999;
        Random rand = new Random();
        return rand.nextInt(limiteSuperior - limiteInferior + 1) + limiteInferior;
    }
    
    private void configuracionModeloTabla(){
        String [] cabecera = {"PARENTESCO" , "NOMBRE" , "DNI", "FECHA DE NACIMIENTO", "GENERO", "TELEFONO", "EMAIL", "DIRECCION"};
        modeloTabla.setColumnIdentifiers(cabecera);        
    }

    protected void desplegarTabla(){  
        Object[] datos = new Object[modeloTabla.getColumnCount()];
        modeloTabla.setRowCount(0);
        for(Familiar f : familiaresAgregados){
            datos[0] = f.getParentesco().trim();
            datos[1] = f.getNombre().trim();
            datos[2] = f.getDni().trim();
            datos[3] = String.valueOf(f.getFechaDeNacimiento()).trim();   
            datos[4] = f.getGenero();
            datos[5] = f.getTelefono();
            datos[6] = f.getEmail();
            datos[7] = f.getDireccion();
            modeloTabla.addRow(datos); 
        }
//        System.out.println(familiaresAgregados);
    }
    protected List<Familiar> getListaFamiliares(){
        return familiaresAgregados;
    }
    
    protected void asignarListaFamiliares(List<Familiar> listaFamilia){
        familiaresAgregados = listaFamilia;
    }
    
    protected void detallesAfiliado(){
        
        Afiliado seleccionado = afiliados.getGestionAfiliados().getLista().get(indiceSeleccionado);
        ingrNombre.setText(seleccionado.getNombre());
        ingrDni.setText(seleccionado.getDni());
        
        String txtDiaA = String.valueOf(seleccionado.getFechaAfiliacion().getDayOfMonth());
        String txtMesA = String.valueOf(seleccionado.getFechaAfiliacion().getMonthValue());
        String txtYearA = String.valueOf(seleccionado.getFechaAfiliacion().getYear());  
        
        String txtFechaA = txtDiaA + "/" + txtMesA + "/" + txtYearA;
        
        String txtDiaN = String.valueOf(seleccionado.getFechaDeNacimiento().getDayOfMonth());
        String txtMesN = String.valueOf(seleccionado.getFechaDeNacimiento().getMonthValue());
        String txtYearN = String.valueOf(seleccionado.getFechaDeNacimiento().getYear());
        
        String txtFechaN = txtDiaN + "/" + txtMesN + "/" + txtYearN;
        
        ingrFa.setText(txtFechaA);
        ingrFn.setText(txtFechaN);
        ingrGenero.setText(seleccionado.getGenero());
        ingrNtel.setText(seleccionado.getTelefono());
        ingrEmail.setText(seleccionado.getEmail());
        ingrDireccion.setText(seleccionado.getDireccion());
        
        familiaresAgregados = seleccionado.getFamiliares();
        desplegarTabla();
    }
    
    protected void setIndice(int indiceSeleccionado){
        this.indiceSeleccionado = indiceSeleccionado;
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel etqBorrar;
    private javax.swing.JLabel etqDetallesFa;
    private javax.swing.JLabel etqEditar;
    private javax.swing.JLabel etqMas;
    private javax.swing.JLabel etqTitulo;
    private javax.swing.JLabel ingrDireccion;
    private javax.swing.JLabel ingrDni;
    private javax.swing.JLabel ingrEmail;
    private javax.swing.JLabel ingrFa;
    private javax.swing.JLabel ingrFn;
    private javax.swing.JLabel ingrGenero;
    private javax.swing.JLabel ingrNombre;
    private javax.swing.JLabel ingrNtel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelNombre;
    private javax.swing.JPanel panelBorrar;
    private javax.swing.JPanel panelDetallesFa;
    private javax.swing.JPanel panelEditar;
    private javax.swing.JPanel panelMas;
    private javax.swing.JTable tablaFamiliares;
    // End of variables declaration//GEN-END:variables
}
