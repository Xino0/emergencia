/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package GUI_Afiliado;

import emergencia.Empresa;
import gestionAfiliado.Familiar;
import gestionAfiliado.Genero;
import gestionAfiliado.Meses;
import java.awt.Color;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Random;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;


/**
 *
 * @author Valentin Roldan
 */
public class AltaFamiliarAfiliado extends javax.swing.JDialog {
    private LocalDate fechaAfiliacion;
    private String parentesco;
    private String nombre;
    private String dni;
    private LocalDate fechaNacimiento;
    private Genero genero;
    private String telefono;
    private String email;
    private String direccion;
    private boolean sePresionaBotonAgregar;
    private final Empresa afiliados;
    int i = 1;
    
    public AltaFamiliarAfiliado(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        afiliados = Empresa.intancia();
        initComponents();
        cargarComboBox();
        sePresionaBotonAgregar = false;
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        etqTitulo = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        nombreTxt = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        dniTxt = new javax.swing.JTextField();
        jSeparator4 = new javax.swing.JSeparator();
        nombreLabel = new javax.swing.JLabel();
        dniLabel = new javax.swing.JLabel();
        fechaNacimientoLabel = new javax.swing.JLabel();
        sexoLabel = new javax.swing.JLabel();
        jSeparator7 = new javax.swing.JSeparator();
        emailTxt = new javax.swing.JTextField();
        numeroTelefonoTxt = new javax.swing.JTextField();
        jSeparator8 = new javax.swing.JSeparator();
        direccionLabel = new javax.swing.JLabel();
        numeroTelefonoLabel = new javax.swing.JLabel();
        jComboGenero = new javax.swing.JComboBox<>();
        direccionLabel1 = new javax.swing.JLabel();
        direccionTxt = new javax.swing.JTextField();
        jSeparator9 = new javax.swing.JSeparator();
        btnAgregarFamiliar = new javax.swing.JButton();
        boxYear = new javax.swing.JComboBox<>();
        boxDia = new javax.swing.JComboBox<>();
        boxMes = new javax.swing.JComboBox<>();
        parentescoTxt = new javax.swing.JTextField();
        jSeparator3 = new javax.swing.JSeparator();
        etqParentesco = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(204, 204, 204));

        etqTitulo.setFont(new java.awt.Font("Swis721 Ex BT", 1, 24)); // NOI18N
        etqTitulo.setText("Registro Familiar Afiliado");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(227, Short.MAX_VALUE)
                .addComponent(etqTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 430, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(83, 83, 83))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(etqTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(12, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 20, 740, 80));

        jPanel4.setBackground(new java.awt.Color(51, 255, 204));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/icono versiofinal.png"))); // NOI18N

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(jLabel1)
                .addContainerGap(26, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1)
                .addContainerGap(460, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 200, 630));

        jPanel5.setBackground(new java.awt.Color(51, 255, 204));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 170, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 630, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(920, 0, 170, 630));

        nombreTxt.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        nombreTxt.setForeground(new java.awt.Color(204, 204, 204));
        nombreTxt.setText("Ingrese su nombre");
        nombreTxt.setBorder(null);
        nombreTxt.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                nombreTxtMousePressed(evt);
            }
        });
        jPanel1.add(nombreTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 180, 420, 30));

        jSeparator1.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 210, 410, 20));

        jSeparator2.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 260, 410, 10));

        dniTxt.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        dniTxt.setForeground(new java.awt.Color(204, 204, 204));
        dniTxt.setText("Ingrese su Dni");
        dniTxt.setBorder(null);
        dniTxt.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                dniTxtMousePressed(evt);
            }
        });
        dniTxt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                dniTxtKeyTyped(evt);
            }
        });
        jPanel1.add(dniTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 230, 420, 30));

        jSeparator4.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 350, 410, 20));

        nombreLabel.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        nombreLabel.setText("NOMBRE");
        jPanel1.add(nombreLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 190, -1, -1));

        dniLabel.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        dniLabel.setText("DNI");
        jPanel1.add(dniLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 240, -1, -1));

        fechaNacimientoLabel.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        fechaNacimientoLabel.setText("FECHA/NACIMIENTO");
        jPanel1.add(fechaNacimientoLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 280, -1, -1));

        sexoLabel.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        sexoLabel.setText("GENERO");
        jPanel1.add(sexoLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 330, -1, -1));

        jSeparator7.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator7, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 450, 410, 20));

        emailTxt.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        emailTxt.setForeground(new java.awt.Color(204, 204, 204));
        emailTxt.setText("Ingrese su email");
        emailTxt.setBorder(null);
        emailTxt.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                emailTxtMousePressed(evt);
            }
        });
        jPanel1.add(emailTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 420, 420, 30));

        numeroTelefonoTxt.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        numeroTelefonoTxt.setForeground(new java.awt.Color(204, 204, 204));
        numeroTelefonoTxt.setText("Ingrese su telefono");
        numeroTelefonoTxt.setBorder(null);
        numeroTelefonoTxt.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                numeroTelefonoTxtMousePressed(evt);
            }
        });
        numeroTelefonoTxt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                numeroTelefonoTxtKeyTyped(evt);
            }
        });
        jPanel1.add(numeroTelefonoTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 370, 420, 30));

        jSeparator8.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator8, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 400, 410, 20));

        direccionLabel.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        direccionLabel.setText("EMAIL");
        jPanel1.add(direccionLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 430, -1, -1));

        numeroTelefonoLabel.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        numeroTelefonoLabel.setText("NRO. TELEFONO");
        jPanel1.add(numeroTelefonoLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 380, -1, -1));

        jComboGenero.setForeground(new java.awt.Color(204, 204, 204));
        jComboGenero.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jComboGenero.setAutoscrolls(true);
        jComboGenero.setBorder(null);
        jComboGenero.setDebugGraphicsOptions(javax.swing.DebugGraphics.NONE_OPTION);
        jPanel1.add(jComboGenero, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 320, 420, 30));

        direccionLabel1.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        direccionLabel1.setText("DIRECCION");
        jPanel1.add(direccionLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 480, -1, -1));

        direccionTxt.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        direccionTxt.setForeground(new java.awt.Color(204, 204, 204));
        direccionTxt.setText("Ingrese su direccion");
        direccionTxt.setBorder(null);
        direccionTxt.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                direccionTxtMousePressed(evt);
            }
        });
        jPanel1.add(direccionTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 470, 420, 30));

        jSeparator9.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator9, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 500, 410, 20));

        btnAgregarFamiliar.setBackground(new java.awt.Color(51, 255, 204));
        btnAgregarFamiliar.setText("Aceptar Familiar");
        btnAgregarFamiliar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarFamiliarActionPerformed(evt);
            }
        });
        jPanel1.add(btnAgregarFamiliar, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 560, 172, 41));

        boxYear.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Año" }));
        jPanel1.add(boxYear, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 280, 80, 25));

        boxDia.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Dia" }));
        jPanel1.add(boxDia, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 280, 80, 25));

        boxMes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Mes" }));
        jPanel1.add(boxMes, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 280, 100, 25));

        parentescoTxt.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        parentescoTxt.setForeground(new java.awt.Color(204, 204, 204));
        parentescoTxt.setText("Ingrese su parentesco con el afiliado");
        parentescoTxt.setBorder(null);
        parentescoTxt.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                parentescoTxtMousePressed(evt);
            }
        });
        jPanel1.add(parentescoTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 130, 420, 30));

        jSeparator3.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 160, 410, 20));

        etqParentesco.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        etqParentesco.setText("PARENTESCO");
        jPanel1.add(etqParentesco, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 140, -1, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void nombreTxtMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_nombreTxtMousePressed
        if (nombreTxt.getText().equals("Ingrese su nombre")) {
            nombreTxt.setText("");
            nombreTxt.setForeground(Color.black);
        } 
        if (dniTxt.getText().isEmpty()) {
            dniTxt.setText("Ingrese su Dni");
            dniTxt.setForeground(Color.gray);
        }


        if (numeroTelefonoTxt.getText().isEmpty()) {
            numeroTelefonoTxt.setText("Ingrese su telefono");
            numeroTelefonoTxt.setForeground(Color.gray);
        }
        if (emailTxt.getText().isEmpty()) {
            emailTxt.setText("Ingrese su email");
            emailTxt.setForeground(Color.gray);
        }
        if (direccionTxt.getText().isEmpty()) {
            direccionTxt.setText("Ingrese su direccion");
            direccionTxt.setForeground(Color.gray);
        }
        if (parentescoTxt.getText().isEmpty()) {
            parentescoTxt.setText("Ingrese su parentesco con el afiliado");
            parentescoTxt.setForeground(Color.gray);
        }                
        
    }//GEN-LAST:event_nombreTxtMousePressed

    private void dniTxtMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dniTxtMousePressed
        if (dniTxt.getText().equals("Ingrese su Dni")) {
            dniTxt.setText("");
            dniTxt.setForeground(Color.black);
        }
        if (nombreTxt.getText().isEmpty()) {
            nombreTxt.setText("Ingrese su nombre");
            nombreTxt.setForeground(Color.gray);
        }   


        if (numeroTelefonoTxt.getText().isEmpty()) {
            numeroTelefonoTxt.setText("Ingrese su telefono");
            numeroTelefonoTxt.setForeground(Color.gray);
        }
        if (emailTxt.getText().isEmpty()) {
            emailTxt.setText("Ingrese su email");
            emailTxt.setForeground(Color.gray);
        }
        if (direccionTxt.getText().isEmpty()) {
            direccionTxt.setText("Ingrese su direccion");
            direccionTxt.setForeground(Color.gray);
        }
        if (parentescoTxt.getText().isEmpty()) {
            parentescoTxt.setText("Ingrese su parentesco con el afiliado");
            parentescoTxt.setForeground(Color.gray);
        }               
        
    }//GEN-LAST:event_dniTxtMousePressed

    private void emailTxtMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_emailTxtMousePressed
        if (emailTxt.getText().equals("Ingrese su email")) {
            emailTxt.setText("");
            emailTxt.setForeground(Color.black);
        }
        if (nombreTxt.getText().isEmpty()) {
            nombreTxt.setText("Ingrese su nombre");
            nombreTxt.setForeground(Color.gray);
        }
        if (dniTxt.getText().isEmpty()) {
            dniTxt.setText("Ingrese su Dni");
            dniTxt.setForeground(Color.gray);
        }


        if (numeroTelefonoTxt.getText().isEmpty()) {
            numeroTelefonoTxt.setText("Ingrese su telefono");
            numeroTelefonoTxt.setForeground(Color.gray);
        }
        if (direccionTxt.getText().isEmpty()) {
            direccionTxt.setText("Ingrese su direccion");
            direccionTxt.setForeground(Color.gray);
        }
        if (parentescoTxt.getText().isEmpty()) {
            parentescoTxt.setText("Ingrese su parentesco con el afiliado");
            parentescoTxt.setForeground(Color.gray);
        }                
        
        
    }//GEN-LAST:event_emailTxtMousePressed

    private void numeroTelefonoTxtMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_numeroTelefonoTxtMousePressed
        if (numeroTelefonoTxt.getText().equals("Ingrese su telefono")) {
            numeroTelefonoTxt.setText("");
            numeroTelefonoTxt.setForeground(Color.black);
        }
        if (nombreTxt.getText().isEmpty()) {
            nombreTxt.setText("Ingrese su nombre");
            nombreTxt.setForeground(Color.gray);
        }
        if (dniTxt.getText().isEmpty()) {
            dniTxt.setText("Ingrese su Dni");
            dniTxt.setForeground(Color.gray);
        }  

     
        if (emailTxt.getText().isEmpty()) {
            emailTxt.setText("Ingrese su email");
            emailTxt.setForeground(Color.gray);
        }
        if (direccionTxt.getText().isEmpty()) {
            direccionTxt.setText("Ingrese su direccion");
            direccionTxt.setForeground(Color.gray);
        }
        if (parentescoTxt.getText().isEmpty()) {
            parentescoTxt.setText("Ingrese su parentesco con el afiliado");
            parentescoTxt.setForeground(Color.gray);
        }
    }//GEN-LAST:event_numeroTelefonoTxtMousePressed

    private void btnAgregarFamiliarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarFamiliarActionPerformed

        int numeroAfiliado = i++;
        
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/M/yyyy");
        String fechaAfiliacionString = String.valueOf(LocalDate.now().getDayOfMonth()) + "/" + String.valueOf(LocalDate.now().getMonthValue()) + "/" + String.valueOf(LocalDate.now().getYear()) ;
        fechaAfiliacion = LocalDate.parse(fechaAfiliacionString, formatter);
        

        try{
            
           int dniAux;
           long telefonoAux;
           
           
           nombre = nombreTxt.getText();
           
           dni = dniTxt.getText();
           telefono = numeroTelefonoTxt.getText();
           
           dniAux = Integer.parseInt(dni);
           telefonoAux = Long.parseLong(telefono);
           
           parentesco = parentescoTxt.getText();            
           String diaString = String.valueOf(boxDia.getSelectedItem());
           String mesString = String.valueOf(boxMes.getSelectedIndex());
           String yearString = String.valueOf(boxYear.getSelectedItem());
           String fechaNacimientoString = diaString + "/" + mesString + "/" + yearString;
           fechaNacimiento = LocalDate.parse(fechaNacimientoString, formatter);

           genero = (Genero)jComboGenero.getSelectedItem();
           telefono = numeroTelefonoTxt.getText();
           email = emailTxt.getText();
           direccion = direccionTxt.getText();
           if (((parentesco.equals("Ingrese su parentesco con el afiliado") || parentesco.isEmpty())||nombre.equals("Ingrese su nombre") || nombre.isEmpty()) || (email.equals("Ingrese su email") || email.isEmpty()) || (direccion.equals("Ingrese su direccion") || direccion.isEmpty())){
                JOptionPane.showMessageDialog(null, "COMPLETE LAS CASILLAS CORRESPONDIENTES");
                return;
           }           
           sePresionaBotonAgregar = true;
//           JOptionPane.showMessageDialog(null, "nombre: " + nombre  + "\nFecha Nacimiento: " + fechaNacimiento + "\nFecha afiliacion: " + fechaAfiliacion + "\nGenero: " + genero + "\nTelefono: " + telefono + "\nEmail: " + email + "\nDireccion: " + direccion, "H", JOptionPane.WARNING_MESSAGE);    
           this.dispose();
           
           telefonoAux = 0;
           dniAux = 0;
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null, "INGRESA UNA FECHA VALIDA", "ERROR", JOptionPane.ERROR_MESSAGE);           
        }       
        
        
        
    }//GEN-LAST:event_btnAgregarFamiliarActionPerformed

    private void direccionTxtMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_direccionTxtMousePressed
        if (direccionTxt.getText().equals("Ingrese su direccion")) {
            direccionTxt.setText("");
            direccionTxt.setForeground(Color.black);
        }
        if (nombreTxt.getText().isEmpty()) {
            nombreTxt.setText("Ingrese su nombre");
            nombreTxt.setForeground(Color.gray);
        }
        if (dniTxt.getText().isEmpty()) {
            dniTxt.setText("Ingrese su Dni");
            dniTxt.setForeground(Color.gray);
        }

        if (numeroTelefonoTxt.getText().isEmpty()) {
            numeroTelefonoTxt.setText("Ingrese su telefono");
            numeroTelefonoTxt.setForeground(Color.gray);
        }
        if (emailTxt.getText().isEmpty())  {
            emailTxt.setText("Ingrese su email");
            emailTxt.setForeground(Color.gray);
        }
        if (parentescoTxt.getText().isEmpty()) {
            parentescoTxt.setText("Ingrese su parentesco con el afiliado");
            parentescoTxt.setForeground(Color.gray);
        }
    }//GEN-LAST:event_direccionTxtMousePressed

    private void parentescoTxtMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_parentescoTxtMousePressed
        if (parentescoTxt.getText().equals("Ingrese su parentesco con el afiliado")) {
            parentescoTxt.setText("");
            parentescoTxt.setForeground(Color.black);
        }
        if (nombreTxt.getText().isEmpty()) {
            nombreTxt.setText("Ingrese su nombre");
            nombreTxt.setForeground(Color.gray);
        }
        if (dniTxt.getText().isEmpty()) {
            dniTxt.setText("Ingrese su Dni");
            dniTxt.setForeground(Color.gray);
        }

        if (numeroTelefonoTxt.getText().isEmpty()) {
            numeroTelefonoTxt.setText("Ingrese su telefono");
            numeroTelefonoTxt.setForeground(Color.gray);
        }
        if (emailTxt.getText().isEmpty())  {
            emailTxt.setText("Ingrese su email");
            emailTxt.setForeground(Color.gray);
        }
        if (direccionTxt.getText().isEmpty()) {
            direccionTxt.setText("Ingrese su direccion");
            direccionTxt.setForeground(Color.gray);
        }
    }//GEN-LAST:event_parentescoTxtMousePressed

    private void dniTxtKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dniTxtKeyTyped
        char c = evt.getKeyChar();
        if (c<'0' || c>'9') evt.consume();
        if(dniTxt.getText().length() >= 8) evt.consume();
    }//GEN-LAST:event_dniTxtKeyTyped

    private void numeroTelefonoTxtKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_numeroTelefonoTxtKeyTyped
        char c = evt.getKeyChar();
        if (c<'0' || c>'9') evt.consume();  
        if(dniTxt.getText().length() >= 19) evt.consume();
    }//GEN-LAST:event_numeroTelefonoTxtKeyTyped
    
    private void cargarComboBox(){
        jComboGenero.setModel(new DefaultComboBoxModel(Genero.values()));
        int q;
        for(q = 1; q <= 31; q++){
            boxDia.addItem(String.valueOf(q));
        }
        boxMes.setModel(new DefaultComboBoxModel(Meses.values()));
        for(q = LocalDate.now().getYear(); q >= 1900; q--){
            boxYear.addItem(String.valueOf(q));
        }
        
    }
    

    private Integer GeneradorNumeroAgente() {
        Integer  limiteInferior = 1000;
        Integer  limiteSuperior = 9999;
        Random rand = new Random();
        return rand.nextInt(limiteSuperior - limiteInferior + 1) + limiteInferior;
    }

    protected Familiar getNuevoFamiliar(){
        Familiar nuevo = new Familiar(parentesco, nombre, fechaNacimiento, dni, genero, telefono, email, direccion);  
//        JOptionPane.showMessageDialog(null, "nombre: " + nombre  + "\nFecha Nacimiento: " + fechaNacimiento + "\nFecha afiliacion: " + fechaAfiliacion + "\nGenero: " + genero + "\nTelefono: " + telefono + "\nEmail: " + email + "\nDireccion: " + direccion, "H", JOptionPane.WARNING_MESSAGE);  
        return nuevo;
    }
    
    protected boolean confirmacion(){
        return sePresionaBotonAgregar;
    }
    
    protected void setParentesco(String parentesco){
        parentescoTxt.setText(parentesco);
        parentescoTxt.setForeground(Color.black);
    }
    
    protected void setTitulo(String titulo){
        etqTitulo.setText(titulo);
    }
    
    protected void setNombreTxt(String nombreTxt) {
        this.nombreTxt.setText(nombreTxt);
        this.nombreTxt.setForeground(Color.black);
    }
    
    protected void setDniTxt(String dniTxt) {
        this.dniTxt.setText(dniTxt);
        this.dniTxt.setForeground(Color.black);
    }
    
    protected void setNumeroTelefonoTxt(String telefonoTxt) {
        this.numeroTelefonoTxt.setText(telefonoTxt);
        this.numeroTelefonoTxt.setForeground(Color.black);
    }
    
    protected void setEmailTxt(String emailTxt) {
        this.emailTxt.setText(emailTxt);
        this.emailTxt.setForeground(Color.black);
    }

    protected void setDireccionTxt(String direccionTxt) {
        this.direccionTxt.setText(direccionTxt);
        this.direccionTxt.setForeground(Color.black);
    }
    
    protected void renombrarBoton(){
        btnAgregarFamiliar.setText("Modificar Familiar");
    }    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> boxDia;
    private javax.swing.JComboBox<String> boxMes;
    private javax.swing.JComboBox<String> boxYear;
    private javax.swing.JButton btnAgregarFamiliar;
    private javax.swing.JLabel direccionLabel;
    private javax.swing.JLabel direccionLabel1;
    private javax.swing.JTextField direccionTxt;
    private javax.swing.JLabel dniLabel;
    private javax.swing.JTextField dniTxt;
    private javax.swing.JTextField emailTxt;
    private javax.swing.JLabel etqParentesco;
    private javax.swing.JLabel etqTitulo;
    private javax.swing.JLabel fechaNacimientoLabel;
    private javax.swing.JComboBox<String> jComboGenero;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JLabel nombreLabel;
    private javax.swing.JTextField nombreTxt;
    private javax.swing.JLabel numeroTelefonoLabel;
    private javax.swing.JTextField numeroTelefonoTxt;
    private javax.swing.JTextField parentescoTxt;
    private javax.swing.JLabel sexoLabel;
    // End of variables declaration//GEN-END:variables
}
