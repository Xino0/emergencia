/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package GUI_Afiliado;

import emergencia.Empresa;
import gestionAfiliado.Familiar;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.swing.JOptionPane;


/**
 *
 * @author Valentin Roldan
 */
public class PanelFamilia extends javax.swing.JDialog {
    private ModeloTabla modeloTabla = new ModeloTabla();
    
    private final Empresa afiliados;
    private List<Familiar> familiaresAgregados;
    int i = 1;
    public PanelFamilia(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        afiliados = Empresa.intancia();
        initComponents();
        configuracionModeloTabla();
        familiaresAgregados = new ArrayList();
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        etqTitulo = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaFamiliares = new javax.swing.JTable();
        panelEditar = new javax.swing.JPanel();
        etqEditar = new javax.swing.JLabel();
        panelMas = new javax.swing.JPanel();
        etqMas = new javax.swing.JLabel();
        panelBorrar = new javax.swing.JPanel();
        etqBorrar = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(204, 204, 204));

        etqTitulo.setFont(new java.awt.Font("Swis721 Ex BT", 1, 24)); // NOI18N
        etqTitulo.setText("Familiares");

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/salud-y-cuidado.png"))); // NOI18N

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/familia.png"))); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 306, Short.MAX_VALUE)
                .addComponent(etqTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 430, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(130, 130, 130))
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                    .addContainerGap(856, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(16, 16, 16)))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(etqTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );

        jPanel1.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 20, 990, 80));

        jPanel4.setBackground(new java.awt.Color(51, 255, 204));

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 60, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 640, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 60, 640));

        jPanel5.setBackground(new java.awt.Color(51, 255, 204));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 60, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 640, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(1030, 0, 60, 640));

        tablaFamiliares.setModel(modeloTabla);
        jScrollPane1.setViewportView(tablaFamiliares);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 120, 970, 460));

        panelEditar.setBackground(new java.awt.Color(51, 255, 204));

        etqEditar.setBackground(new java.awt.Color(51, 255, 204));
        etqEditar.setForeground(new java.awt.Color(0, 255, 153));
        etqEditar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        etqEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/editar.png"))); // NOI18N
        etqEditar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                etqEditarMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                etqEditarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                etqEditarMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                etqEditarMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                etqEditarMouseReleased(evt);
            }
        });

        javax.swing.GroupLayout panelEditarLayout = new javax.swing.GroupLayout(panelEditar);
        panelEditar.setLayout(panelEditarLayout);
        panelEditarLayout.setHorizontalGroup(
            panelEditarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(etqEditar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
        );
        panelEditarLayout.setVerticalGroup(
            panelEditarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(etqEditar, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
        );

        jPanel1.add(panelEditar, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 590, -1, -1));

        panelMas.setBackground(new Color(24, 194, 119));

        etqMas.setBackground(new java.awt.Color(0, 204, 204));
        etqMas.setForeground(new java.awt.Color(0, 204, 204));
        etqMas.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        etqMas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/signoMas.png"))); // NOI18N
        etqMas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                etqMasMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                etqMasMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                etqMasMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                etqMasMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                etqMasMouseReleased(evt);
            }
        });

        javax.swing.GroupLayout panelMasLayout = new javax.swing.GroupLayout(panelMas);
        panelMas.setLayout(panelMasLayout);
        panelMasLayout.setHorizontalGroup(
            panelMasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(etqMas, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
        );
        panelMasLayout.setVerticalGroup(
            panelMasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(etqMas, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
        );

        jPanel1.add(panelMas, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 590, -1, -1));

        panelBorrar.setBackground(new Color(173,15,15));

        etqBorrar.setBackground(new java.awt.Color(0, 204, 204));
        etqBorrar.setForeground(new java.awt.Color(0, 204, 204));
        etqBorrar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        etqBorrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/basura.png"))); // NOI18N
        etqBorrar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                etqBorrarMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                etqBorrarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                etqBorrarMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                etqBorrarMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                etqBorrarMouseReleased(evt);
            }
        });

        javax.swing.GroupLayout panelBorrarLayout = new javax.swing.GroupLayout(panelBorrar);
        panelBorrar.setLayout(panelBorrarLayout);
        panelBorrarLayout.setHorizontalGroup(
            panelBorrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(etqBorrar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
        );
        panelBorrarLayout.setVerticalGroup(
            panelBorrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(etqBorrar, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
        );

        jPanel1.add(panelBorrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(970, 590, -1, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void etqEditarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etqEditarMouseClicked
        if(tablaFamiliares.getSelectedRow() == -1){
            JOptionPane.showMessageDialog(null, "Debe seleccionar un familiar", "Error",JOptionPane.WARNING_MESSAGE);              
        }
        else{
            AltaFamiliarAfiliado ventanaModificar = new AltaFamiliarAfiliado(new javax.swing.JFrame(),true);
            
            ventanaModificar.setTitulo("Modificar Familiar del afiliado");
            
            Familiar modificar = familiaresAgregados.get(tablaFamiliares.getSelectedRow());
            ventanaModificar.setParentesco(modificar.getParentesco());
            ventanaModificar.setNombreTxt(modificar.getNombre());
            ventanaModificar.setDniTxt(modificar.getDni());
            ventanaModificar.setNumeroTelefonoTxt(modificar.getTelefono());
            ventanaModificar.setEmailTxt(modificar.getEmail());
            ventanaModificar.setDireccionTxt(modificar.getDireccion());
            ventanaModificar.renombrarBoton();
            
            
            ventanaModificar.setVisible(true);
            
            familiaresAgregados.set(tablaFamiliares.getSelectedRow(), ventanaModificar.getNuevoFamiliar());
            desplegarTabla();
        }
    }//GEN-LAST:event_etqEditarMouseClicked

    private void etqEditarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etqEditarMouseEntered
        panelEditar.setBackground(new Color(0,255,255));
    }//GEN-LAST:event_etqEditarMouseEntered

    private void etqEditarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etqEditarMouseExited
        panelEditar.setBackground(new Color(0,204,204));
    }//GEN-LAST:event_etqEditarMouseExited

    private void etqEditarMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etqEditarMousePressed
        panelEditar.setBackground(new Color(17,141,172));
    }//GEN-LAST:event_etqEditarMousePressed

    private void etqEditarMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etqEditarMouseReleased
        panelEditar.setBackground(new Color(0,255,255));
    }//GEN-LAST:event_etqEditarMouseReleased

    private void etqMasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etqMasMouseClicked
        AltaFamiliarAfiliado ventanaAgregarFamiliar = new AltaFamiliarAfiliado(new javax.swing.JFrame(),true);
        ventanaAgregarFamiliar.setVisible(true);
        if(ventanaAgregarFamiliar.confirmacion()){
           familiaresAgregados.add(ventanaAgregarFamiliar.getNuevoFamiliar());
           desplegarTabla();
        }
    }//GEN-LAST:event_etqMasMouseClicked

    private void etqMasMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etqMasMouseEntered
        panelMas.setBackground(new Color(8,255,0));
    }//GEN-LAST:event_etqMasMouseEntered

    private void etqMasMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etqMasMouseExited
        panelMas.setBackground(new Color(24,194,19));
    }//GEN-LAST:event_etqMasMouseExited

    private void etqMasMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etqMasMousePressed
        panelMas.setBackground(new Color(15,123,12));
    }//GEN-LAST:event_etqMasMousePressed

    private void etqMasMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etqMasMouseReleased
        panelMas.setBackground(new Color(8,255,0));
    }//GEN-LAST:event_etqMasMouseReleased

    private void etqBorrarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etqBorrarMouseClicked
        if(tablaFamiliares.getSelectedRow() == -1){
            JOptionPane.showMessageDialog(null, "Debe seleccionar un familiar", "Error",JOptionPane.WARNING_MESSAGE);              
        }
        else{
            familiaresAgregados.remove(tablaFamiliares.getSelectedRow());
            desplegarTabla();
        }
    }//GEN-LAST:event_etqBorrarMouseClicked

    private void etqBorrarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etqBorrarMouseEntered
        panelBorrar.setBackground(new Color(255,0,0));
    }//GEN-LAST:event_etqBorrarMouseEntered

    private void etqBorrarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etqBorrarMouseExited
        panelBorrar.setBackground(new Color(173,15,15));
    }//GEN-LAST:event_etqBorrarMouseExited

    private void etqBorrarMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etqBorrarMousePressed
        panelBorrar.setBackground(new Color(142,23,23));
    }//GEN-LAST:event_etqBorrarMousePressed

    private void etqBorrarMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etqBorrarMouseReleased
        panelBorrar.setBackground(new Color(255,0,0));
    }//GEN-LAST:event_etqBorrarMouseReleased
    

    

    private Integer GeneradorNumeroAgente() {
        Integer  limiteInferior = 1000;
        Integer  limiteSuperior = 9999;
        Random rand = new Random();
        return rand.nextInt(limiteSuperior - limiteInferior + 1) + limiteInferior;
    }
    
    private void configuracionModeloTabla(){
        String [] cabecera = {"PARENTESCO" , "NOMBRE" , "DNI", "FECHA DE NACIMIENTO", "GENERO", "TELEFONO", "EMAIL", "DIRECCION"};
        modeloTabla.setColumnIdentifiers(cabecera);        
    }

    protected void desplegarTabla(){  
        Object[] datos = new Object[modeloTabla.getColumnCount()];
        modeloTabla.setRowCount(0);
        for(Familiar f : familiaresAgregados){
            datos[0] = f.getParentesco().trim();
            datos[1] = f.getNombre().trim();
            datos[2] = f.getDni().trim();
            datos[3] = String.valueOf(f.getFechaDeNacimiento()).trim();   
            datos[4] = f.getGenero();
            datos[5] = f.getTelefono();
            datos[6] = f.getEmail();
            datos[7] = f.getDireccion();
            modeloTabla.addRow(datos); 
        }
//        System.out.println(familiaresAgregados);
    }
    protected List<Familiar> getListaFamiliares(){
        return familiaresAgregados;
    }
    
    protected void asignarListaFamiliares(List<Familiar> listaFamilia){
        familiaresAgregados = listaFamilia;
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    protected javax.swing.JLabel etqBorrar;
    protected javax.swing.JLabel etqEditar;
    protected javax.swing.JLabel etqMas;
    protected javax.swing.JLabel etqTitulo;
    protected javax.swing.JLabel jLabel1;
    protected javax.swing.JLabel jLabel2;
    protected javax.swing.JPanel jPanel1;
    protected javax.swing.JPanel jPanel3;
    protected javax.swing.JPanel jPanel4;
    protected javax.swing.JPanel jPanel5;
    protected javax.swing.JScrollPane jScrollPane1;
    protected javax.swing.JPanel panelBorrar;
    protected javax.swing.JPanel panelEditar;
    protected javax.swing.JPanel panelMas;
    protected javax.swing.JTable tablaFamiliares;
    // End of variables declaration//GEN-END:variables
}
