
package GUI_Afiliado;

import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Valentin Roldan
 */
public class ModeloTabla extends DefaultTableModel{
        @Override
        public boolean isCellEditable(int filas, int columnas){
          return columnas == 4;  
        }
}
