/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionAfiliado;

import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author Valentin Roldan
 */
public class Familiar extends Persona{
    private String parentesco;

    public Familiar(String parentesco, String nombre, LocalDate fechaDeNacimiento, String dni, Genero genero, String telefono, String email, String direccion) {
        super(nombre, fechaDeNacimiento, dni, genero, telefono, email, direccion);
        this.parentesco = parentesco;
    }

    
    public String getParentesco() {
        return parentesco;
    }

    public void setParentesco(String parentesco) {
        this.parentesco = parentesco;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Familiar other = (Familiar) obj;
        return Objects.equals(this.parentesco, other.parentesco);
    }
    
    

    @Override
    public String toString() {
        return "Familia{" + "parentesco=" + parentesco + '}';
    }
    
    
}
