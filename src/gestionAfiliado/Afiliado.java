/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionAfiliado;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Roldan
 */
public class Afiliado extends Persona{
    private final int numeroAfiliado;
    private final List<Familiar> familiares;
    private final LocalDate fechaAfiliacion;
    private static int i = 1;
    public Afiliado(List<Familiar> familiares, LocalDate fechaAfiliacion, String nombre, LocalDate fechaDeNacimiento, String dni, Genero genero, String telefono, String email, String direccion) {
        super(nombre, fechaDeNacimiento, dni, genero, telefono, email, direccion);

        this.numeroAfiliado = i++;
        this.familiares = familiares;
        this.fechaAfiliacion = fechaAfiliacion;
    }


    public Integer getNumeroAfiliado() {
        return numeroAfiliado;
    }


    public List<Familiar> getFamiliares() {
        return familiares;
    }



    public LocalDate getFechaAfiliacion() {
        return fechaAfiliacion;
    }
    
    public void agregarFamiliar(Familiar nuevo){
        familiares.add(nuevo);
        
    }
    
    
    public void eliminarFamiliar(Familiar aux){
        familiares.remove(aux);
    }
    
   
    public void mostrarItemsFamiliar(){
        for(Familiar aux: familiares){
            System.out.println(aux);
        }
    }
    
    public void buscarFamiliar(Familiar busqueda){
        
        for(Familiar aux: familiares){
            if(busqueda==aux){
                System.out.println(aux);
                
            }
            
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Afiliado other = (Afiliado) obj;
        return Objects.equals(this.getDni(), other.getDni());
    }

    @Override
    public String toString() {
        return super.toString()+" N° Afiliado: " + numeroAfiliado + " Fecha De Afiliacion: " + fechaAfiliacion;
    }

    
    

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.numeroAfiliado);
        hash = 29 * hash + Objects.hashCode(this.familiares);
        hash = 29 * hash + Objects.hashCode(this.fechaAfiliacion);
        return hash;
    }
    
    public void menosUno(){
        this.i--;
    }
}
