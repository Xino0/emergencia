
package gestionAfiliado;

/**
 *
 * @author Valentin Roldan
 */
public enum Meses {
    Mes,
    Enero,Febrero,Marzo,
    Abril,Mayo,Junio,
    Julio,Agosto,Septiembre,
    Octubre,Noviembre,Diciembre;
}
