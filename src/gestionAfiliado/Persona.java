/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionAfiliado;

import java.time.LocalDate;
import java.time.Period;
import java.util.Objects;

/**
 *
 * @author Valentin Roldan
 */
public abstract class Persona {
    private String nombre;
    private LocalDate fechaDeNacimiento;
    private String dni;
    private Genero genero;
    private String telefono; 
    private String email;
    private String direccion;
    

    public Persona(String nombre, LocalDate fechaDeNacimiento, String dni, Genero sexo, String telefono, String email, String direccion) {
        this.nombre = nombre;
        this.fechaDeNacimiento = fechaDeNacimiento;
        this.dni = dni;
        this.genero = sexo;
        this.telefono = telefono;
        this.email = email;
        this.direccion = direccion;
    }

    

    

   

    public String getNombre() {
        return nombre;
    }

    public String getGenero(){
        return genero.name();
    }

    public LocalDate getFechaDeNacimiento() {
        return fechaDeNacimiento;
    }

    public void setFechaDeNacimiento(LocalDate fechaDeNacimiento) {
        this.fechaDeNacimiento = fechaDeNacimiento;
    }
    
    
    public int calcularEdad(){
        int edad;
        LocalDate fechaActual= LocalDate.now();
        Period periodo= Period.between(fechaActual, fechaDeNacimiento);
        edad=periodo.getYears();
        return Math.abs(edad);
    }

    public String getDni() {
        return dni;
    }

    public String getTelefono() {
        return telefono;
    }
    

  

    public String getEmail() {
        return email;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    

    public void setDni(String dni) {
        this.dni = dni;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    

    public void setEmail(String email) {
        this.email = email;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Genero getSexo() {
        return genero;
    }

    public void setSexo(Genero sexo) {
        this.genero = sexo;
    }

    
    
    
    
    @Override
    public String toString() {
        return "Nombre: " + nombre+ " Direccion: " + direccion ;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Persona other = (Persona) obj;
     
         if (!Objects.equals(this.dni, other.dni)) {
            return false;
        }
    
        return Objects.equals(this.direccion, other.direccion);
    }

   
    
    
    
}
    



    
    

