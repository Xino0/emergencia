
package gestionAfiliado;

import Excepciones.AltaException;
import Excepciones.InexistenteException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Valentin Roldan
 */
public class RepositorioGestionAfiliados {
    private final List<Afiliado> afiliados;
    public RepositorioGestionAfiliados() {
        
           
        this.afiliados = new ArrayList<>();
        
      

    }
    
    public void agregar(Afiliado nuevo) throws AltaException, InexistenteException{
      
        Afiliado busqueda=this.buscarAfiliado(nuevo);
        
        if(busqueda==null){
            
            afiliados.add(nuevo);
            
        }
        else if(busqueda.equals(nuevo)){
         
            throw new AltaException("El DNI del Afiliado ya se encuentra registrado");
        }
       
        else  afiliados.add(nuevo);
        
    }
    
    
    public void eliminarAfiliado(Afiliado eliminar)throws InexistenteException{
        Afiliado buscar=this.buscarAfiliado(eliminar);
        if(buscar==null){
            throw new InexistenteException("El Empleado a eliminar no existe.");
        }
        else{
            JOptionPane.showMessageDialog(null, "Se eliminó el afiliado " + buscar.getNombre() + " con DNI: " + buscar.getDni(), "ELIMINADO",JOptionPane.WARNING_MESSAGE);   
            afiliados.remove(eliminar);
        }
            
    }
    
    public Afiliado buscarAfiliado(Afiliado buscar) throws InexistenteException{
        
        if(buscar!=null){
            
            for(Afiliado aux: afiliados){ 
                 
                 if(buscar.equals(aux)){
                    return buscar;
                 
                 }
            }
    
        }
        else throw new InexistenteException("Afiliado Inexistente");
           
        return null;            
        
    }
    
    public void modificarAfiliado(Afiliado Original, Afiliado Modificar) throws InexistenteException {
        
        Afiliado buscar = this.buscarAfiliado(Original);
        if(buscar == null){
            throw new InexistenteException("El Afiliado a modificar no existe.");
        }
        else{
            System.out.println(afiliados);
            System.out.println(buscar);
            
            int index = afiliados.indexOf(buscar);
            System.out.println(index);
            afiliados.set(index, Modificar);
            JOptionPane.showMessageDialog(null, "Se modifico el afiliado", "EXITO", JOptionPane.INFORMATION_MESSAGE);
        }
        
    }
    
    public List<Afiliado> getLista(){
        return afiliados;
    }

    public Afiliado buscar(Integer buscar) throws InexistenteException {
          
        if(buscar!=null){
            
            for(Afiliado aux: afiliados){ 
                 
                 if(buscar.equals(aux.getNumeroAfiliado())){
                    return aux;
                 
                 }
            }
    
        }
        else throw new InexistenteException("Afiliado Inexistente");
           
        return null; 
    }
}