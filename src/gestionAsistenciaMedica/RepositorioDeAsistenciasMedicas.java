/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionAsistenciaMedica;


import Excepciones.AltaException;
import Excepciones.InexistenteException;
import gestionAfiliado.Afiliado;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author david
 */
public class RepositorioDeAsistenciasMedicas {
    
    private List<SolicitudAsistenciaMedica> SolicitudAsistenciaMedica;

    public RepositorioDeAsistenciasMedicas() {
        this.SolicitudAsistenciaMedica = new ArrayList<SolicitudAsistenciaMedica>();
        
    }

    public List<SolicitudAsistenciaMedica> getSolicitudAsistenciaMedica() {
        return SolicitudAsistenciaMedica;
    }

    public void setSolicitudAsistenciaMedica(List<SolicitudAsistenciaMedica> SolicitudAsistenciaMedica) {
        this.SolicitudAsistenciaMedica = SolicitudAsistenciaMedica;
    }
    
    
    
    
    public void agregarSolicitudAsistenciaMedica(SolicitudAsistenciaMedica agregar) throws AltaException, InexistenteException{
      
        SolicitudAsistenciaMedica busqueda=this.buscar(agregar);
        
        if(busqueda==null){
            
            SolicitudAsistenciaMedica.add(agregar);
            
        }
        else if(busqueda.equals(agregar)){
         
            throw new AltaException("La solicitud ya se encuentra registrado");
        }
       
        else  SolicitudAsistenciaMedica.add(agregar);
      
        

         
        
        

        
    }
    
    
    public void eliminarSolicitudAsistenciaMedica(SolicitudAsistenciaMedica eliminar)throws InexistenteException{
        SolicitudAsistenciaMedica buscar=this.buscar(eliminar);
        if(buscar==null){
            throw new InexistenteException("La solicitud a eliminar no existe.");
        }
        else{
            System.out.println("Se elimino la solicitud del afiliado: "+eliminar.getCliente().getNombre());
            SolicitudAsistenciaMedica.remove(eliminar);
        }
            
    }
    
    public SolicitudAsistenciaMedica buscar(SolicitudAsistenciaMedica buscar) throws InexistenteException{
        
        if(buscar!=null){
            
            for(SolicitudAsistenciaMedica aux: SolicitudAsistenciaMedica){ 
                 
                 if(buscar.equals(aux)){
                    
                    return buscar;
                 
                 }
            }
    
        }
        else if(buscar==null){
             throw new InexistenteException("Solicitud Inexistente"); 
        }
            
         return null;            
        
    }
    
    public void modificarSolicitudAsistenciaMedica(SolicitudAsistenciaMedica Original, SolicitudAsistenciaMedica Modificar) throws InexistenteException {
        
        SolicitudAsistenciaMedica buscar = this.buscar(Original);
        if(buscar == null){
            throw new InexistenteException("La solicitud a modificar no existe.");
        }
        else{
            int index = SolicitudAsistenciaMedica.indexOf(buscar);
            SolicitudAsistenciaMedica.set(index, Modificar);
            System.out.println("Se modificó la solicitud del afiliado: " + Modificar.getCliente().getNombre());
        }
       
        
    }
    
              
       
       
       
      
       
   
    
}
