/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionAsistenciaMedica;

import Excepciones.AltaException;
import Excepciones.InexistenteException;
import gestionAfiliado.Afiliado;
import gestionEmpleados.Chofer;
import gestionEmpleados.Doctor;
import gestionMoviles.Movil;
import gestionEmpleados.Empleado;
import gestionEmpleados.Enfermero;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import java.util.Objects;

/**
 *
 * @author david
 */
public class SolicitudAsistenciaMedica  {
    private TipoSolicitud tipoSolicitud;
    private Afiliado cliente;
    private LocalDate fechaSolicitud;
    private Movil ambulancia;
    private Doctor doctor;
    private Enfermero enfermero;
    private Chofer chofer;
    private List empleados;
    
    private String Diagnositco;
    
    
    public SolicitudAsistenciaMedica(TipoSolicitud tipoSolicitud, Afiliado cliente, LocalDate fechaSolicitud, Movil ambulancia,Doctor medico,Enfermero enfermero, Chofer conductor){
        this.tipoSolicitud = tipoSolicitud;
        this.cliente = cliente;
        this.fechaSolicitud = fechaSolicitud;
        this.ambulancia = ambulancia;
        this.doctor=medico;
        this.enfermero=enfermero;
        this.chofer=conductor;
        empleados= new ArrayList<>();
        empleados.add(medico);
        empleados.add(enfermero);
        empleados.add(conductor);
       
        
    }

    public TipoSolicitud getTipoSolicitud(){
        return tipoSolicitud;
    }

    public void setTipoSolicitud(TipoSolicitud tipoSolicitud) {
        this.tipoSolicitud = tipoSolicitud;
        
    }

    

    public String getDiagnositco() {
        return Diagnositco;
    }

    public void setDiagnositco(String Diagnositco) {
        this.Diagnositco = Diagnositco;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Enfermero getEnfermero() {
        return enfermero;
    }

    public void setEnfermero(Enfermero enfermero) {
        this.enfermero = enfermero;
    }

    public Chofer getChofer() {
        return chofer;
    }

    public void setChofer(Chofer chofer) {
        this.chofer = chofer;
    }
    
    

  
    public Afiliado getCliente() {
        return cliente;
    }

    public void setCliente(Afiliado cliente) {
        this.cliente = cliente;
    }

    public LocalDate getFechaSolicitud() {
        return fechaSolicitud;
    }

    public void setFechaSolicitud(LocalDate fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    public Movil getAmbulancia() {
        return ambulancia;
    }

    public void setAmbulancia(Movil ambulancia) {
        this.ambulancia = ambulancia;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }
    
    public void agregarEmpleado(Empleado agregar) throws AltaException, InexistenteException{
      
        Empleado busqueda=this.buscar(agregar);
        
        if(busqueda==null){
            
            empleados.add(agregar);
            
        }
        else if(busqueda.equals(agregar)){
         
            throw new AltaException("El Beneficiario ya se encuentra registrado");
        }
       
        else  empleados.add(agregar);
      
        

         
        
        

        
    }
    
    
    public void eliminarEmpleado(Empleado eliminar)throws InexistenteException{
        Empleado buscar=this.buscar(eliminar);
        if(buscar==null){
            throw new InexistenteException("El Empleado a eliminar no existe.");
        }
        else{
            System.out.println("Se elimino el Beneficiario: "+eliminar.getNombre());
            empleados.remove(eliminar);
        }
            
    }
    
    public Empleado buscar(Empleado buscar) throws InexistenteException{
        
        if(buscar!=null){
            
            for(Object aux: empleados){ 
                 
                 if(buscar.equals(aux)){
                    
                    return buscar;
                 
                 }
            }
    
        }
        else if(buscar==null){
             throw new InexistenteException("Empleado Inexistente"); 
        }
            
         return null;            
        
    }
    
    public void modificarEmpleado(Empleado Original, Empleado Modificar) throws InexistenteException {
        
        Empleado buscar = this.buscar(Original);
        if(buscar == null){
            throw new InexistenteException("El Empleado a modificar no existe.");
        }
        else{
            int index = empleados.indexOf(buscar);
            empleados.set(index, Modificar);
            System.out.println("Se modificó el empleado: " + Modificar.getNombre());
        }
        
    }

    
   @Override
    public boolean equals(Object obj) {
    if (this == obj) {
        return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
        return false;
    }

    SolicitudAsistenciaMedica other = (SolicitudAsistenciaMedica) obj;

    // Compara el cliente y la fecha
    if (cliente != null && cliente.equals(other.cliente) &&
        fechaSolicitud != null && fechaSolicitud.equals(other.fechaSolicitud)) {
        return true;
    }

    return false;
    }


  
 

    

    
    
  
}
