/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelos.Tablas;

import gestionAsistenciaMedica.SolicitudAsistenciaMedica;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.swing.event.EventListenerList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author david
 */
public class ModeloTablaSolicitudes extends AbstractTableModel{
    private final Class[] tipoColumnas;
    private final String[] titleColumnas;

    private List<SolicitudAsistenciaMedica> solicitudes;

    public ModeloTablaSolicitudes() {
       
        this.titleColumnas = new String[]{"FECHA SOLICITUD","NOMBRE AFILIADO","DIRECCION","N°AMBULANCIA","NOMBRE MEDICO","NOMBRE ENFERMERO","NOMBRE CHOFER"};
        this.tipoColumnas = new Class[] {LocalDate.class,String.class,String.class,Integer.class,String.class,String.class,String.class};
        this.solicitudes = new ArrayList<>();
        
        
    }

    public List<SolicitudAsistenciaMedica> getSolicitudes() {
        return solicitudes;
    }

    public void setSolicitudes(List<SolicitudAsistenciaMedica> solicitudes) {
        this.solicitudes = solicitudes;
    }

   
    public EventListenerList getListenerList() {
        return listenerList;
    }

    public void setListenerList(EventListenerList listenerList) {
        this.listenerList = listenerList;
    }

    
    
   
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return tipoColumnas[columnIndex];
    }
    
    
    
    @Override
    public String getColumnName(int column) {
         return titleColumnas[column];
    }
    
    
    
    @Override
    public int getRowCount() {
        return solicitudes.size();
    }

    @Override
    public int getColumnCount() {
        return titleColumnas.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            case 0:
                return solicitudes.get(rowIndex).getFechaSolicitud(); //Fecha de solicitud
            case 1:
                return solicitudes.get(rowIndex).getCliente().getNombre(); //Nombre de afiliado
            case 2:
                return solicitudes.get(rowIndex).getCliente().getDireccion(); //direccion
            case 3:
                return solicitudes.get(rowIndex).getAmbulancia().getNumeroVehiculo(); //Numero de Ambulancia 
            case 4:
                return solicitudes.get(rowIndex).getDoctor().getNombre(); //Nombre Medico
              
            case 5:
                return solicitudes.get(rowIndex).getEnfermero().getNombre(); //Nombre Enfermero
            case 6:
                return solicitudes.get(rowIndex).getChofer().getNombre(); //Nombre chofer
          
        }
        return null;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        switch(columnIndex){
            case 0:
                solicitudes.get(rowIndex).setFechaSolicitud(LocalDate.EPOCH);
                break;
            case 1:
                break;
            case 2:
                solicitudes.get(rowIndex).getCliente().setNombre(aValue.toString());
                break;
            case 3:
                solicitudes.get(rowIndex).getCliente().setDireccion(aValue.toString());
                break;        
            case 4:
                solicitudes.get(rowIndex).getAmbulancia().setNumeroVehiculo( aValue.toString());
                break;
            case 5:
                solicitudes.get(rowIndex).getDoctor().setNombre(aValue.toString());
                break;
            case 6:
                solicitudes.get(rowIndex).getEnfermero().setNombre(aValue.toString());
                break;
            case 7:
                solicitudes.get(rowIndex).getChofer().setNombre(aValue.toString());
                break;    
       
            default:;
        }
        this.fireTableCellUpdated(rowIndex, columnIndex);
        this.fireTableRowsUpdated(rowIndex, rowIndex);
         
    }

   
    
    

    
}
