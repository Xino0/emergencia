/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelos.Tablas;

import gestionEmpleados.Empleado;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.swing.event.EventListenerList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author david
 */
public class ModeloTablaEmpleados extends AbstractTableModel{
    private final Class[] tipoColumnas;
    private final String[] titleColumnas;
    private List<Empleado> empleados;

    public ModeloTablaEmpleados() {
        this.titleColumnas = new String[]{"N°AGENTE","PUESTO","Nombre","EDAD","DNI","N°TELEFONO","EMAIL","DIRECCION"};
        this.tipoColumnas = new Class[]{Integer.class,Object.class,String.class,int.class,Integer.class,Long.class,String.class,String.class};
        this.empleados = new ArrayList<>();
    }

    public List<Empleado> getEmpleados() {
        return empleados;
    }

    public void setEmpleados(List<Empleado> empleados) {
        this.empleados = empleados;
    }

    public EventListenerList getListenerList() {
        return listenerList;
    }

    public void setListenerList(EventListenerList listenerList) {
        this.listenerList = listenerList;
    }

    
    
   
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return tipoColumnas[columnIndex];
    }
    
    
    
    @Override
    public String getColumnName(int column) {
         return titleColumnas[column];
    }
    
    
    
    @Override
    public int getRowCount() {
        return empleados.size();
    }

    @Override
    public int getColumnCount() {
        return titleColumnas.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            case 0:
                return empleados.get(rowIndex).getNumeroAgente(); //numero de agente
            case 1:
                return empleados.get(rowIndex).getClass().getSimpleName(); //tipo de empleado  
            case 2:
                return empleados.get(rowIndex).getNombre(); //nombre
            case 3:
                return empleados.get(rowIndex).calcularEdad(); //edad   
            case 4:
                return empleados.get(rowIndex).getDni(); //DNI
            case 5:
                return empleados.get(rowIndex).getTelefono(); //Telefono
            case 6:
                return empleados.get(rowIndex).getEmail(); //Email
            case 7:
                return empleados.get(rowIndex).getDireccion(); //Direccion   
        }
        return null;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        switch(columnIndex){
            case 0:
                empleados.get(rowIndex).setNumeroAgente((Integer) aValue);
                break;
            case 1:
                break;
            case 2:
                empleados.get(rowIndex).setNombre(aValue.toString());
                break;
            case 3:
                empleados.get(rowIndex).setFechaDeNacimiento(LocalDate.MIN);
                break;        
            case 4:
                empleados.get(rowIndex).setDni(aValue.toString());
                break;
            case 5:
                empleados.get(rowIndex).setTelefono(aValue.toString());
                break;
            case 6:
                empleados.get(rowIndex).setEmail(aValue.toString());
                break;
            case 7:
                empleados.get(rowIndex).setDireccion(aValue.toString());
                break;    
                
                
           
            default:;
        }
        this.fireTableCellUpdated(rowIndex, columnIndex);
        this.fireTableRowsUpdated(rowIndex, rowIndex);
         
    }

   
    
    

    
}
