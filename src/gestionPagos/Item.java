/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionPagos;

/**
 *
 * @author david
 */
public class Item {
    private Integer codigo;
    private Float precioUnitario;

    public Item(Integer codigo, Float precioUnitario) {
        this.codigo = codigo;
        this.precioUnitario = precioUnitario;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public Float getPrecioUnitario() {
        return precioUnitario;
    }

    @Override
    public String toString() {
        return "Item{" + "codigo=" + codigo + ", precioUnitario=" + precioUnitario + '}';
    }
 
}
