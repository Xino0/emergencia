/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionPagos;

/**
 *
 * @author xino
 */
import Excepciones.AltaException;
import Excepciones.InexistenteException;
import gestionAfiliado.Afiliado;
import java.time.LocalDate;
import java.util.ArrayList;

import java.util.Objects;

public class Factura  {
    private Afiliado cliente;
    private ArrayList<Item> items;
    private String formaPago;
    private double numeroFactura;
    private LocalDate fechaRegistrada;
    
    // Constructor
    public Factura(Afiliado cliente, ArrayList<Item> items, float totalPago, String formaPago, double numeroFactura, LocalDate fechaRegistrada) {
        this.cliente = cliente;
        this.items = new ArrayList<>();
        this.formaPago = formaPago;
        this.numeroFactura = numeroFactura;
        this.fechaRegistrada = fechaRegistrada;
    }
    // Getters y setters para los atributos
    public Afiliado getCliente() {
        return cliente;
    }
    public void setCliente(Afiliado cliente) {
        this.cliente = cliente;
    }
    public String getFormaPago() {
        return formaPago;
    }
    public void setFormaPago(String formaPago) {
        this.formaPago = formaPago;
    }
    public double getNumeroFactura() {
        return numeroFactura;
    }
    public void setNumeroFactura(double numeroFactura) {
        this.numeroFactura = numeroFactura;
    }
    public LocalDate getFechaRegistrada() {
        return fechaRegistrada;
    }
    public void setFechaRegistrada(LocalDate fechaRegistrada) {
        this.fechaRegistrada = fechaRegistrada;
    }
    // Método para calcular el monto total de la factura
    public float montoTotal(){
        float monto=0f;
        
        for(Item aux: items){
            monto+=aux.getPrecioUnitario();
        }
        
        return monto;
    }
    // Agrega un item a la factura
    public void agregarItem(Item agregar) throws AltaException, InexistenteException{
        Item busqueda=this.buscar(agregar);
        
        if(busqueda==null){  
            items.add(agregar); 
        }
        else if(busqueda.equals(agregar)){
            throw new AltaException("El Item ya se encuentra registrado");
        }
        else  items.add(agregar);
    }
    // Elimina un item de la factura
    public void eliminarItem(Item eliminar)throws InexistenteException{
        Item buscar=this.buscar(eliminar);
        if(buscar==null){
            throw new InexistenteException("El Item a eliminar no existe.");
        }
        else{
            System.out.println("Se elimino el Item de codigo: "+eliminar.getCodigo());
            items.remove(eliminar);
        }     
    }
    // Busca un item en la factura
    public Item buscar(Item buscar) throws InexistenteException{
        
        if(buscar!=null){
            
            for(Item aux: items){ 
                 
                 if(buscar.equals(aux)){
                    
                    return buscar;
                 
                 }
            }
        }
        else if(buscar==null){
             throw new InexistenteException("Item Inexistente"); 
        }
        return null;            
    }
    // Modifica un item en la factura
    public void modificarItem(Item Original, Item Modificar) throws InexistenteException {
        Item buscar = this.buscar(Original);
        
        if(buscar == null){
            throw new InexistenteException("El Item a modificar no existe.");
        }
        else{
            int index = items.indexOf(buscar);
            items.set(index, Modificar);
            System.out.println("Se modificó el Item de codigo: " + Modificar.getCodigo());
        }
    }
    // Implementacion del metodo hashCode
    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }
    // Implementacion del metodo equals
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Factura other = (Factura) obj;
        if (Double.doubleToLongBits(this.numeroFactura) != Double.doubleToLongBits(other.numeroFactura)) {
            return false;
        }
      
        return Objects.equals(this.fechaRegistrada, other.fechaRegistrada);
    }
    // Implementacion del metodo toString
    @Override
    public String toString() {
        return "Factura{" + "cliente=" + cliente + ", items=" + items + ", formaPago=" + formaPago + ", numeroFactura=" + numeroFactura + ", fechaRegistrada=" + fechaRegistrada + '}';
    }
}
