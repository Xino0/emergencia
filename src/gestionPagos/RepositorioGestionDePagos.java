/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionPagos;

/**
 *
 * @author xino
 */
import Excepciones.AltaException;
import Excepciones.InexistenteException;

import java.util.ArrayList;

public class RepositorioGestionDePagos {
    private ArrayList<Factura> facturas;

    // Constructor
    public RepositorioGestionDePagos() {       
        facturas = new ArrayList<>();
    }
    
    // Agrega una factura al repositorio
    public void agregarFactura(Factura agregar) throws AltaException, InexistenteException{
        Factura busqueda=this.buscar(agregar);
        
        if(busqueda==null){
            facturas.add(agregar);
        }
        else if(busqueda.equals(agregar)){
            throw new AltaException("La factura ya se encuentra registrado");
        }
        else  facturas.add(agregar);
    }
    
    // Elimina una factura del repositorio
    public void eliminarFactura(Factura eliminar)throws InexistenteException{
        Factura buscar=this.buscar(eliminar);
        if(buscar==null){
            throw new InexistenteException("La Factura a eliminar no existe.");
        }
        else{
            System.out.println("Se elimino la Factura de numero : "+eliminar.getNumeroFactura());
            facturas.remove(eliminar);
        }    
    }
    
    // Busca una factura en el repositorio
    public Factura buscar(Factura buscar) throws InexistenteException{
        if(buscar!=null){
            for(Factura aux: facturas){ 
                 if(buscar.equals(aux)){
                    return buscar;
                 }
            }
        }
        else if(buscar==null){
             throw new InexistenteException("Factura Inexistente"); 
        }          
         return null;
    }
    
    // Modifica una factura en el repositorio
    public void modificarFactura(Factura Original, Factura Modificar) throws InexistenteException {
        
        Factura buscar = this.buscar(Original);
        if(buscar == null){
            throw new InexistenteException("Factura a modificar no existe.");
        }
        else{
            int index = facturas.indexOf(buscar);
            facturas.set(index, Modificar);
            System.out.println("Se modificó la factura de numero: " + Modificar.getNumeroFactura());
        }
        
    }

}  