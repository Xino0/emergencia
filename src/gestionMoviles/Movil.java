package gestionMoviles;

import java.util.Objects;

public class Movil {
    private String numeroVehiculo;
    private String patente;
    private Boolean disponibilidad;
    private String marca;
    private String modelo;
    private String anio;

    public Movil(String numeroVehiculo, String patente, Boolean disponibilidad, String marca, String modelo, String anio) {
        this.numeroVehiculo = numeroVehiculo;
        this.patente = patente;
        this.disponibilidad = disponibilidad;
        this.marca = marca;
        this.modelo = modelo;
        this.anio = anio;
    }

    public String getNumeroVehiculo() {
        return numeroVehiculo;
    }

    public void setNumeroVehiculo(String numeroVehiculo) {
        this.numeroVehiculo = numeroVehiculo;
    }

    public String getPatente() {
        return patente;
    }

    public void setPatente(String patente) {
        this.patente = patente;
    }

    public Boolean getDisponibilidad() {
        return disponibilidad;
    }

    public void setDisponibilidad(Boolean disponibilidad) {
        this.disponibilidad = disponibilidad;
    }
    
    public void changeDisponibilidad(){
        if(disponibilidad == true){
            disponibilidad = false;
        }else{
            disponibilidad = true;
        }
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getAnio() {
        return anio;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }

    @Override
    public String toString() {
        return "Numero de Vehiculo: " + numeroVehiculo + " Patente: " + patente ;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.numeroVehiculo);
        hash = 97 * hash + Objects.hashCode(this.patente);
        hash = 97 * hash + Objects.hashCode(this.disponibilidad);
        hash = 97 * hash + Objects.hashCode(this.marca);
        hash = 97 * hash + Objects.hashCode(this.modelo);
        hash = 97 * hash + Objects.hashCode(this.anio);
        return hash;
    }
    
    @Override
    public boolean equals(Object otro) {
        // 1. Optimización:
        if (this == otro) {
            return true;
        }
        // 2. No null:
        if (otro == null) {
            return false;
        }
        // 3: Clase igual:
        if (getClass() != otro.getClass()) {
            return false;
        }
        // 4. Comparación campo-a-campo:
        Movil otroMovil = (Movil) otro;
        
        // Utiliza la comparación de la superclase para comparar los atributos heredados
        if (super.equals(otroMovil)) {
            return false;
        }

        return otroMovil.getMarca().equals(this.getMarca())
            && otroMovil.getModelo().equals(this.getModelo())
            && otroMovil.getAnio().equals(this.getAnio());
    }
    
}
