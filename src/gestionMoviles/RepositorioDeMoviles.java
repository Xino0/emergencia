package gestionMoviles;

import java.util.ArrayList;
import Excepciones.AltaException;
import Excepciones.InexistenteException;

public class RepositorioDeMoviles {
    private ArrayList<Movil> moviles;
    
    public RepositorioDeMoviles() {
        this.moviles = new ArrayList<>();
    }

    public ArrayList<Movil> getMoviles() {
        return moviles;
    }

    public void agregar(Movil nuevo) throws AltaException, InexistenteException {
        Movil busqueda = this.buscar(nuevo);

        if (busqueda == null) {
            moviles.add(nuevo);
        } else if (busqueda.equals(nuevo)) {
            throw new AltaException("El vehiculo ya se encuentra registrado.");
        } else {
            moviles.add(nuevo);
        }
    }
    
    public void eliminar(Movil eliminar) throws InexistenteException { 
        Movil buscar = this.buscar(eliminar);
        if (buscar == null) {
            throw new InexistenteException("Seleccione un vehiculo a eliminar.");
        } else {
            System.out.println("Se elimino el vehiculo con patente: " + eliminar.getPatente());
            moviles.remove(eliminar);
        }
    }
    
    public void eliminarIndex(int index) throws InexistenteException{
        Movil buscar = this.buscar(moviles.get(index));
        if (buscar == null) {
            throw new InexistenteException("Seleccione un vehiculo a eliminar.");
        } else {
            System.out.println("Se elimino el vehiculo con patente: " + moviles.get(index).getPatente());
            moviles.remove(moviles.get(index));
        }
    }
    
    public Movil buscar(Movil buscar){
        if (buscar != null) {
            for (Movil aux : moviles) {
                if (buscar.equals(aux)) {
                    return buscar;
                }
            }
        }
        return null;
    }
    
    public Movil buscar(String buscar) throws InexistenteException,AltaException{
        
        if(buscar!=null){
            
            for(Movil aux: moviles){ 
                 
                 if(buscar.equals(aux.getNumeroVehiculo()) && aux.getDisponibilidad()==true){
                    
                    return aux;
                 
                }
                 else if(buscar.equals(aux.getNumeroVehiculo()) && aux.getDisponibilidad()==false){
                     throw new AltaException("La Ambulancia No esta disponible.");
                 }
            }
    
        }
        else if(buscar==null){
             throw new InexistenteException("Empleado Inexistente"); 
        }
            
         return null;            
        
    }
    
    public void modificarMovil(Movil Original, Movil Modificar) throws InexistenteException {
        Movil buscar = this.buscar(Original);
        if (buscar == null) {
            throw new InexistenteException("El vehiculo a modificar no existe.");
        } else {
            int index = moviles.indexOf(buscar);
            moviles.set(index, Modificar);
            System.out.println("Se modifico el vehiculo con patente: " + Modificar.getPatente());
        }
    }   
}
