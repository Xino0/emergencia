/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Excepciones;

/**
 *
 * @author david
 */
public class InexistenteException extends Exception{

    public InexistenteException(String message) {
        super(message);
    }
    
    
    
}
